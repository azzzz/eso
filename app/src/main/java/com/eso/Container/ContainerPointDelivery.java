package com.eso.Container;

import com.eso.Common.Constants;
import com.google.gson.annotations.SerializedName;

public class ContainerPointDelivery {

    @SerializedName(Constants.Json_trip_id)
    public int PointDelivery_IdTrip;

    @SerializedName(Constants.Json_date_trip)
    public String PointDelivery_DateTrip;

    @SerializedName(Constants.Json_statement)
    public String PointDelivery_DescriptionTrip;

    public int PointDelivery_Id;

    @SerializedName(Constants.Json_client_id)
    public int PointDelivery_IdClient;

    @SerializedName(Constants.Json_clientName)
    public String PointDelivery_NameClient;

    @SerializedName(Constants.Json_delivery_code)
    public String PointDelivery_Code;

    @SerializedName(Constants.Json_delivery_town)
    public String PointDelivery_Town;

    @SerializedName(Constants.Json_delivery_street)
    public String PointDelivery_Street;

    @SerializedName(Constants.Json_delivery_newConstruction)
    public String PointDelivery_NewConstruction;

    @SerializedName(Constants.Json_id_country)
    public int PointDelivery_IdCountry;

    @SerializedName(Constants.Json_delivery_country)
    public String PointDelivery_Country;

    public int PointDelivery_UserId;

    public int get_IdTrip() {
        return PointDelivery_IdTrip;
    }
    public String get_DateTrip() {
        return PointDelivery_DateTrip;
    }
    public int get_IdClient() {
        return PointDelivery_IdClient;
    }
    public String get_DescriptionTrip() {
        return PointDelivery_DescriptionTrip;
    }
    public int get_PointDelivery_DescriptionTrip() {
        return PointDelivery_Id;
    }
    public String get_NameClient() {
        return PointDelivery_NameClient;
    }
    public String get_Code() {
        return PointDelivery_Code;
    }
    public int get_IdCountry() {
        return PointDelivery_IdCountry;
    }
    public String get_Country() {
        return PointDelivery_Country;
    }
    public String get_Street() {
        return PointDelivery_Street;
    }
    public String get_NewConstruction() {
        return PointDelivery_NewConstruction;
    }
    public String get_Town() {
        return PointDelivery_Town;
    }
    public int get_PointDelivery_UserId() {
        return PointDelivery_UserId;
    }


    public void set_IdTrip(int id) {
        this.PointDelivery_IdTrip = id;
    }
    public void set_IdClient(int id) {
        this.PointDelivery_IdClient = id;
    }
    public void set_DescriptionTrip(String tripid) {
        this.PointDelivery_DescriptionTrip = tripid;
    }
    public void set_Id(int id) {
        this.PointDelivery_Id = id;
    }
    public void set_NameClient(String Client) {
        this.PointDelivery_NameClient = Client;
    }
    public void set_Code(String Code) {
        this.PointDelivery_Code = Code;
    }
    public void set_Town(String Town) {
        this.PointDelivery_Town = Town;
    }
    public void set_Street(String Street) {
        this.PointDelivery_Street = Street;
    }
    public void set_NewConstruction(String NewConstruction) {
        this.PointDelivery_NewConstruction = NewConstruction;
    }
    public void set_Country(String Country) {
        this.PointDelivery_Country = Country;
    }
    public void set_PointDelivery_UserId(int iduser) {
        this.PointDelivery_UserId = iduser;
    }

}
