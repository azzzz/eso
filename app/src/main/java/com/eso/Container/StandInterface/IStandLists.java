package com.eso.Container.StandInterface;

import com.eso.Container.ContainerWindowStand_left;
import com.eso.Container.ContainerWindowStand_recive;

import java.util.List;

public interface IStandLists {

    List<ContainerWindowStand_left> GetListContainerWindowStand_left();
    List<ContainerWindowStand_recive> GetListContainerWindowStand_recive();
}
