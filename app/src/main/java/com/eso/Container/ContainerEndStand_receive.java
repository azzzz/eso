package com.eso.Container;

import com.eso.Common.Constants;
import com.google.gson.annotations.SerializedName;

public class ContainerEndStand_receive {

    @SerializedName(Constants.Json_stands_id)
    public int ContainerEndStand_StandId;

    @SerializedName(Constants.Json_stands_name)
    public String ContainerEndStand_StandName;

    @SerializedName("punkt_odebrania_id_klient")
    public long ContainerEndStand_reciveingPoint;

    @SerializedName(Constants.Json_delivery_code)
    public String PointDelivery_Code;

    @SerializedName(Constants.Json_delivery_town)
    public String PointDelivery_Town;

    @SerializedName(Constants.Json_delivery_street)
    public String PointDelivery_Street;

    @SerializedName("data_odebrania")
    public String PointDelivery_reciveingDate;

    @SerializedName(Constants.Json_delivery_newConstruction)
    public String PointDelivery_newConstruction;

}
