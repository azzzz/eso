package com.eso.Container;

public class ContainerWindowStand_recive {

    public int Stand_Id;
    public int Stand_TripId;
    public int Stand_ClientId;
    public int Stand_IsAddedByUser;
    public int Stand_UserId;
    public String Stand_NameWindow;
    public String Stand_DateClick;
    public int Stand_Status;
    private boolean Stand_isSelected = false;

    public void setStand_isSelected(boolean stand_isSelected) {
        Stand_isSelected = stand_isSelected;
    }
    public boolean isStand_isSelected() {
        return Stand_isSelected;
    }




    public ContainerWindowStand_recive() {
    }

    public int getId() {
        return Stand_Id;
    }

    public int gettripid() {
        return Stand_TripId;
    }

    public int getclientid() {
        return Stand_ClientId;
    }

    public int getisadded() {
        return Stand_IsAddedByUser;
    }

    public int getuserid() {
        return Stand_UserId;
    }

    public String getnamewindow() {
        return Stand_NameWindow;
    }

    public String getdateclick() {
        return Stand_DateClick;
    }

    public int getstatus() {
        return Stand_Status;
    }



    public void setId(int id) {
        this.Stand_Id = id;
    }

    public void settripid(int tripid) {
        this.Stand_TripId = tripid;
    }

    public void setclientid(int clientid) {
        this.Stand_ClientId = clientid;
    }

    public void setisadded(int isadded) {
        this.Stand_IsAddedByUser = isadded;
    }

    public void setuserid(int userid) {
        this.Stand_UserId = userid;
    }

    public void setnamewindow(String namewindow) {
        this.Stand_NameWindow = namewindow;
    }

    public void setdateclick(String dateclick) {
        this.Stand_DateClick = dateclick;
    }

    public void setstatus(int status) {
        this.Stand_Status = status;
    }



}
