package com.eso.Container;

import com.eso.Common.Constants;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContainerEndDelivery {

    @SerializedName(Constants.Json_token)
    public String PointDeliveryEnd_Token;

    @SerializedName(Constants.Json_driver_id)
    public long PointDeliveryEnd_IdDriver;

    @SerializedName(Constants.Json_trip_id)
    public long PointDeliveryEnd_IdTrip;

    @SerializedName("zostawione")
    public List<ContainerEndStand_left> PointDeliveryEnd_ListLeftStand;

    @SerializedName("odebrane")
    public List<ContainerEndStand_receive> PointDeliveryEnd_ListReciveStand;

    @SerializedName("odebrane_user")
    public List<ContainerEndStand_receive> PointDeliveryEnd_ListReciveStand_ByAddUser;

}
