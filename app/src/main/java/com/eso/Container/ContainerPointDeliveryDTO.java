package com.eso.Container;

import com.eso.Common.Constants;
import com.eso.Container.ContainerEndStand_left;
import com.eso.Container.ContainerEndStand_receive;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContainerPointDeliveryDTO {

    @SerializedName(Constants.Json_client_id)
    public int PointDelivery_IdClient;

    @SerializedName(Constants.Json_clientName)
    public String PointDelivery_NameClient;

    @SerializedName(Constants.Json_delivery_code)
    public String PointDelivery_Code;

    @SerializedName(Constants.Json_delivery_town)
    public String PointDelivery_Town;

    @SerializedName(Constants.Json_delivery_street)
    public String PointDelivery_Street;

    @SerializedName(Constants.Json_delivery_newConstruction)
    public String PointDelivery_NewConstruction;

}
