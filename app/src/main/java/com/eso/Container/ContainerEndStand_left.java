package com.eso.Container;

import com.eso.Common.Constants;
import com.eso.Container.ContainerVersion;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContainerEndStand_left {

    @SerializedName(Constants.Json_stands_id)
    public int ContainerEndStand_StandId;

    @SerializedName(Constants.Json_stands_name)
    public String ContainerEndStand_StandName;

    @SerializedName("punkt_pozostawienia_id_klient")
    public long ContainerEndStand_LeavingPoint;

    @SerializedName(Constants.Json_delivery_code)
    public String PointDelivery_Code;

    @SerializedName(Constants.Json_delivery_town)
    public String PointDelivery_Town;

    @SerializedName(Constants.Json_delivery_street)
    public String PointDelivery_Street;

    @SerializedName("data_pozostawienia")
    public String PointDelivery_LeavingDate;

    @SerializedName(Constants.Json_delivery_newConstruction)
    public String PointDelivery_newConstruction;



}
