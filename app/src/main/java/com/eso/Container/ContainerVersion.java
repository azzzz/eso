package com.eso.Container;


import com.google.gson.annotations.SerializedName;

public class ContainerVersion {

    @SerializedName("wersja")
    public Long Version;
    @SerializedName("komunikat")
    public String Description;

    public Long version() {
        return Version;
    }
    public String description() {
        return Description;
    }
}
