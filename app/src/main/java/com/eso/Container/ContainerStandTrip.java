package com.eso.Container;

import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class ContainerStandTrip {
    public int Stand_Id;
    public int Stand_TripId;
    public int Stand_ClientId;
    public int Stand_Isaddbyuser;
    public String Stand_NameWindow;
    public int Stand_Status;
    public Date Stand_date;
    public int Stand_UserId;
}
