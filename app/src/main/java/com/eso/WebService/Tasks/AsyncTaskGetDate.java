package com.eso.WebService.Tasks;

import com.eso.Common.Constants;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.eso.Common.Constants.STRING_EMPTY;




public class AsyncTaskGetDate extends AsyncTaskBase
{
    public String Result = STRING_EMPTY;
    public Date ServerDate;

    @Override
    public String Execute()
    {
        SoapObject request = new SoapObject(NAMESPACE, METHOD);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER12);
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;


        String result = STRING_EMPTY;

        try
        {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapObject response = (SoapObject) envelope.bodyIn;
            if (response != null)
            {
                result = response.getProperty(0).toString();
            }
            else
            {
                result = Constants.Response_NoResponse;
            }
        }
        catch (Exception e)
        {
            result = Constants.Response_ConnectionFailure;
        }



        return result;
    }

    @Override
    protected void onPostExecute(String result)
    {
        PostExecute(result);
    }

    public void PostExecute(String result)
    {
        this.Result = result;
        Boolean isDefaultResponse = false;
        if (this.Result.equals(Constants.Response_NoResponse))
        {
            isDefaultResponse = true;
        }
        if (this.Result.equals(Constants.Response_ConnectionFailure))
        {
            isDefaultResponse = true;
        }

        if (isDefaultResponse == false)
        {
            this.ServerDate = null;
            //result = CryptoSecurity.decrypt(result);
            SimpleDateFormat df = new SimpleDateFormat(Constants.ServerDateFormat);
            try
            {
                this.ServerDate = df.parse(result);
            }
            catch (ParseException e)
            {
                e.printStackTrace();
            }
            if (ServerDate != null)
            {
                this.TaskSuccess = true;
                this.Result = result;
            }
        }
        this.TaskCompleted = true;
        return;
    }
}