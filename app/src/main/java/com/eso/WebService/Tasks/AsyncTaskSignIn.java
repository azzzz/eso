package com.eso.WebService.Tasks;

import com.eso.Common.Constants;
import com.eso.WebService.Interfaces.ISignIn;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import static com.eso.Common.Constants.STRING_EMPTY;


public class AsyncTaskSignIn extends AsyncTaskBase
{
    public String Result = STRING_EMPTY;
    private String login = "";
    private String password = "";
    public ISignIn Parent;


    public void SetParameters(String Login, String Password, ISignIn parent)
    {
        this.login = Login;
        this.password = Password;
        this.Parent = parent;
    }

    @Override
    protected String Execute()
    {
        String result = STRING_EMPTY;


        SoapObject request = new SoapObject(NAMESPACE, METHOD);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER12);
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;

        PropertyInfo LoginProperty = new PropertyInfo();
        LoginProperty.setNamespace(NAMESPACE);
        LoginProperty.setType(PropertyInfo.STRING_CLASS);
        LoginProperty.setName("Login");
        LoginProperty.setValue(this.login);
        request.addProperty(LoginProperty);

        PropertyInfo PasswordProperty = new PropertyInfo();
        PasswordProperty.setNamespace(NAMESPACE);
        PasswordProperty.setType(PropertyInfo.STRING_CLASS);
        PasswordProperty.setName("Pass");
        PasswordProperty.setValue(this.password);
        request.addProperty(PasswordProperty);


        try
        {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapObject response = (SoapObject) envelope.bodyIn;
            if (response != null)
            {
                result = response.getProperty(0).toString();
            }
            else
            {
                result = Constants.Response_NoResponse;
            }
        }
        catch (Exception e)
        {
            result = Constants.Response_ConnectionFailure;
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result)
    {
        this.Result = result;
        Boolean isDefaultResponse = false;

        this.TaskCompleted = true;
        Parent.OnAsyncTaskSignIn(this);
        return;
    }

}
