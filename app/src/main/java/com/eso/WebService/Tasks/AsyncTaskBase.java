package com.eso.WebService.Tasks;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.eso.Common.Constants;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import static com.eso.Common.Constants.STRING_EMPTY;



public class AsyncTaskBase
        extends AsyncTask<Void, String, String>
{


    protected String URL;
    protected String NAMESPACE;
    protected String SOAP_ACTION;
    protected String METHOD;

    protected Context context = null;
    public Boolean TaskCompleted;
    public Boolean TaskSuccess;

    public void SetContext(Context context)
    {
        this.context = context;
    }

    public void Init(String URL, String NAMESPACE, String SOAP_ACTION, String METHOD)
    {
        this.TaskCompleted = false;
        this.TaskSuccess = false;
        this.URL = URL;
        this.NAMESPACE = NAMESPACE;
        this.SOAP_ACTION = SOAP_ACTION;
        this.METHOD = METHOD;
    }

    protected String Execute()
    {
        SoapObject request = new SoapObject(NAMESPACE, METHOD);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER12);
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;

        String result = STRING_EMPTY;
        try
        {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapObject response = (SoapObject) envelope.bodyIn;
            if (response != null)
            {
                result = response.getProperty(0).toString();
            }
            else
            {
                result=Constants.Response_NoResponse;
            }
        }
        catch (Exception e)
        {
            result= Constants.Response_ConnectionFailure;
        }
        return result;
    }


    protected String doInBackground(Void... params)
    {
        String result = Execute();
        return result;
    }

    protected void WriteResponse(String result)
    {
        if (this.context != null)
        {
            try
            {
                Toast.makeText(this.context, result, Toast.LENGTH_LONG).show();
            }
            catch (Exception e)
            {

            }
        }
    }

    @Override
    protected void onProgressUpdate(String... progress)
    {
        return;
    }

    @Override
    protected void onPostExecute(String result)
    {
        this.TaskCompleted = true;
        return;
    }
}