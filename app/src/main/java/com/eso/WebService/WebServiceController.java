package com.eso.WebService;

import android.content.Context;

import com.eso.Common.Constants;
import com.eso.WebService.Interfaces.ISignIn;
import com.eso.WebService.Tasks.AsyncTaskGetDate;
import com.eso.WebService.Tasks.AsyncTaskSignIn;


public class WebServiceController
{
    //private static String URL = Constants.WebServiceURL;
    private static String NAMESPACE = "http://panoptika.pl";

    private static String URL = Constants.WebServiceURL;
    //private static String NAMESPACE = "http://secumedia.pl";

    public static String METHOD_Login = "Login";
    public static String SOAP_ACTION_Login = NAMESPACE + "/" + METHOD_Login;


    public static String METHOD_AndroidIndependantGetDate = "Connected";
    public static String SOAP_ACTION_AndroidIndependantGetDate = NAMESPACE + "/" + METHOD_AndroidIndependantGetDate;



    public static void GetDate(Context context)
    {
        AsyncTaskGetDate task = new AsyncTaskGetDate();
        task.Init(URL, NAMESPACE, SOAP_ACTION_AndroidIndependantGetDate, METHOD_AndroidIndependantGetDate);
        task.SetContext(context);
        try
        {
            task.execute(null, null, null);
        }
        catch (Exception e)
        {

        }
    }

    public static void SignIn(Context context, String LoginSet, String PasswordSet, ISignIn parent)
    {
        AsyncTaskSignIn task = new AsyncTaskSignIn();
        task.Init(URL, NAMESPACE, SOAP_ACTION_Login, METHOD_Login);
        task.SetContext(context);
        //String encryptedLoginSet = CryptoSecurity.encrypt(LoginSet);
        //String encryptedPasswordSet = CryptoSecurity.encrypt(PasswordSet);
        task.SetParameters(LoginSet, PasswordSet, parent);
        try {
            task.execute(null, null, null);
        } catch (Exception e) {

        }
    }


}
