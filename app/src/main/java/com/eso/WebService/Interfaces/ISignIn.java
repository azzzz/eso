package com.eso.WebService.Interfaces;



import com.eso.WebService.Tasks.AsyncTaskSignIn;

/**
 * Created by Marcin on 2017-10-04.
 */

public interface ISignIn
{
    public void OnAsyncTaskSignIn(AsyncTaskSignIn caller);
}
