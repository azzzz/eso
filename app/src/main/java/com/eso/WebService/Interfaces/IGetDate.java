package com.eso.WebService.Interfaces;


import com.eso.WebService.Tasks.AsyncTaskGetDate;

/**
 * Created by Marcin on 2017-10-04.
 */

public interface IGetDate
{
    public void OnAsyncTaskGetDate(AsyncTaskGetDate caller);
}
