package com.eso.DI.Infrastructure;

import com.eso.Common.Constants;
import com.eso.Common.FileLoadSave;
import com.eso.DI.MyApplication;

import java.util.List;

import javax.inject.Inject;

public class LoadAndSaveDI {

    @Inject
    public LoadAndSaveDI(){
    }

    public List<String> LoadUserToken() {
        FileLoadSave fileLoadSave = new FileLoadSave();
        return  fileLoadSave.Read(MyApplication.getAppContext(), Constants.UserTokenNameFile);
    }

}
