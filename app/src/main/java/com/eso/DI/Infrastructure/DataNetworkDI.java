package com.eso.DI.Infrastructure;

import android.content.Context;
import android.widget.Toast;

import com.eso.Activites.DriverAdressClientActivty;
import com.eso.Common.Constants;
import com.eso.Container.ContainerEndDelivery;
import com.eso.Container.ContainerEndStand_left;
import com.eso.Container.ContainerEndStand_receive;
import com.eso.Container.ContainerPointDelivery;
import com.eso.Container.ContainerStandTrip;
import com.eso.Container.ContainerVersion;
import com.eso.DI.MyApplication;
import com.eso.Database.DatabaseHelper;
import com.eso.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

public class DataNetworkDI {

    private DatabaseHelper db;

    @Inject
    public DataNetworkDI() {
        db = new DatabaseHelper(MyApplication.getAppContext());
    }

    public List<ContainerPointDelivery> GeneratelistdataContainerPointDelivery(String result, int IdUser) {

        List<ContainerPointDelivery> listdataContainerPointDelivery = new ArrayList<>();
        try {
            //pocztek do "komunikat"
            JSONObject resultJson_trip = new JSONObject(result);
            //Punkty
            JSONObject JObject_point = resultJson_trip.getJSONObject(Constants.Json_point);
            Iterator<String> iter = JObject_point.keys();

            while (iter.hasNext()) {
                ContainerPointDelivery containerPointDelivery = new ContainerPointDelivery();
                String key = iter.next();
                //UserId jest zapisywan do pliku
                containerPointDelivery.PointDelivery_UserId = IdUser;
                //Poczatek request
                if (resultJson_trip.has(Constants.Json_trip_id))
                    containerPointDelivery.PointDelivery_IdTrip = resultJson_trip.getInt(Constants.Json_trip_id);

                if (resultJson_trip.has(Constants.Json_date_trip))
                    containerPointDelivery.PointDelivery_DateTrip = resultJson_trip.getString(Constants.Json_date_trip);

                if (resultJson_trip.has(Constants.Json_statement))
                    containerPointDelivery.PointDelivery_DescriptionTrip = resultJson_trip.getString(Constants.Json_statement);


                //Od numer punktu
                JSONObject JObject_pointnumber = JObject_point.getJSONObject(key);
                if (JObject_pointnumber.has(Constants.Json_client_id))
                    containerPointDelivery.PointDelivery_IdClient = JObject_pointnumber.getInt(Constants.Json_client_id);

                if (JObject_pointnumber.has(Constants.Json_clientName))
                    containerPointDelivery.PointDelivery_NameClient = JObject_pointnumber.getString(Constants.Json_clientName);

                //adres
                JSONObject JObject_client = JObject_pointnumber.getJSONObject(Constants.Json_adress);
                if (JObject_client.has(Constants.Json_delivery_code))
                    containerPointDelivery.PointDelivery_Code = JObject_client.getString(Constants.Json_delivery_code);

                if (JObject_client.has(Constants.Json_delivery_town))
                    containerPointDelivery.PointDelivery_Town = JObject_client.getString(Constants.Json_delivery_town);

                if (JObject_client.has(Constants.Json_delivery_street))
                    containerPointDelivery.PointDelivery_Street = JObject_client.getString(Constants.Json_delivery_street);

                if (JObject_client.has(Constants.Json_delivery_newConstruction))
                    containerPointDelivery.PointDelivery_NewConstruction = JObject_client.getString(Constants.Json_delivery_newConstruction);

                if (JObject_client.has(Constants.Json_delivery_country))
                    containerPointDelivery.PointDelivery_Country = JObject_client.getString(Constants.Json_delivery_country);


                listdataContainerPointDelivery.add(containerPointDelivery);
                //sprawdzam czy dane alementy są już w bazie danych, jeśli nie to dodaj do sqlLite
                if (db.checkIfExistPointDelivery(containerPointDelivery.PointDelivery_IdTrip, IdUser, containerPointDelivery.PointDelivery_IdClient) == true) {
                    long j = db.insertPointDelivery(containerPointDelivery.PointDelivery_IdTrip,containerPointDelivery.PointDelivery_DateTrip, containerPointDelivery.PointDelivery_DescriptionTrip, containerPointDelivery.PointDelivery_Id, containerPointDelivery.PointDelivery_IdClient, containerPointDelivery.PointDelivery_NameClient, containerPointDelivery.PointDelivery_Code, containerPointDelivery.PointDelivery_Town, containerPointDelivery.PointDelivery_Street, containerPointDelivery.PointDelivery_NewConstruction, containerPointDelivery.PointDelivery_Country, IdUser);
                }
            }
        } catch (Exception e) {

        }
        return listdataContainerPointDelivery;
    }

    public List<ContainerStandTrip> GenerateContainerStandTrip_ClientStand(String result, int IdUser) {
        List<ContainerStandTrip> containerStandTrip_ClientStand = new ArrayList<>();

        try {
            //pocztek do "komunikat"
            JSONObject resultJson_trip = new JSONObject(result);
            //Punkty
            JSONObject JObject_point = resultJson_trip.getJSONObject(Constants.Json_point);
            Iterator<String> iter = JObject_point.keys();
            while (iter.hasNext()) {
                ContainerPointDelivery containerPointDelivery = new ContainerPointDelivery();
                String key = iter.next();
                //Od numer punktu
                JSONObject JObject_pointnumber = JObject_point.getJSONObject(key);
                //Stojaki Klienta
                JSONArray JSONArray_standClient = JObject_pointnumber.getJSONArray(Constants.Json_standsclient);
                for (int current = 0; current < JSONArray_standClient.length(); current++) {
                    ContainerStandTrip containerStandTrip = new ContainerStandTrip();
                    JSONObject JObject_standClient = JSONArray_standClient.getJSONObject(current);

                    containerStandTrip.Stand_UserId = IdUser;
                    containerStandTrip.Stand_Status = Constants.StatusStand_client;
                    if (JObject_standClient.has(Constants.Json_stands_id))
                        containerStandTrip.Stand_Id = JObject_standClient.getInt(Constants.Json_stands_id);
                    if (resultJson_trip.has(Constants.Json_trip_id))
                        containerStandTrip.Stand_TripId = resultJson_trip.getInt(Constants.Json_trip_id);
                    if (JObject_pointnumber.has(Constants.Json_client_id))
                        containerStandTrip.Stand_ClientId = JObject_pointnumber.getInt(Constants.Json_client_id);
                    if (JObject_standClient.has(Constants.Json_stands_name))
                        containerStandTrip.Stand_NameWindow = JObject_standClient.getString(Constants.Json_stands_name);
                    containerStandTrip_ClientStand.add(containerStandTrip);

                    //sprawdzam czy dane alementy są już w bazie danych, jeśli nie to dodaj do sqlLite
                    if (db.checkIfExistStands(containerStandTrip.Stand_Id, containerStandTrip.Stand_TripId,Constants.AddbyUser_False, containerStandTrip.Stand_ClientId) == true) {
                        long k = db.insertStand(containerStandTrip.Stand_Id, containerStandTrip.Stand_TripId, containerStandTrip.Stand_ClientId,Constants.AddbyUser_False, IdUser, containerStandTrip.Stand_NameWindow, Calendar.getInstance().getTime(), 1);
                    }
                }
            }
        } catch (Exception e) {

        }
        return containerStandTrip_ClientStand;
    }

    public List<ContainerStandTrip> GenerateContainerStandTrip_FreeStand(String result, int IdUser) {
        List<ContainerStandTrip> containerStandTrip_FreeStand = new ArrayList<>();

        try {
            //pocztek do "komunikat"
            JSONObject resultJson_trip = new JSONObject(result);
            //Punkty
            JSONArray JArray_standsFree = resultJson_trip.getJSONArray(Constants.Json_stands);
            for (int current = 0; current < JArray_standsFree.length(); current++) {
                ContainerStandTrip containerStandTrip = new ContainerStandTrip();
                JSONObject JObject_standFree = JArray_standsFree.getJSONObject(current);
                containerStandTrip.Stand_UserId = IdUser;
                containerStandTrip.Stand_Status = Constants.StatusStand_free;
                containerStandTrip.Stand_ClientId = 0;
                if (JObject_standFree.has(Constants.Json_stands_id))
                    containerStandTrip.Stand_Id = JObject_standFree.getInt(Constants.Json_stands_id);
                if (resultJson_trip.has(Constants.Json_trip_id))
                    containerStandTrip.Stand_TripId = resultJson_trip.getInt(Constants.Json_trip_id);
                if (JObject_standFree.has(Constants.Json_stands_name))
                    containerStandTrip.Stand_NameWindow = JObject_standFree.getString(Constants.Json_stands_name);
                containerStandTrip_FreeStand.add(containerStandTrip);
                //sprawdzam czy dane alementy są już w bazie danych, jeśli nie to dodaj do sqlLite
                if (db.checkIfExistStands_onlyStandID(containerStandTrip.Stand_Id,Constants.AddbyUser_False) == true) {
                    long k = db.insertStand(containerStandTrip.Stand_Id, 0, 0,Constants.AddbyUser_False, IdUser, containerStandTrip.Stand_NameWindow, Calendar.getInstance().getTime(), 0);
                }
            }
        }
        catch (Exception e) {

        }
        return containerStandTrip_FreeStand;
    }

    public String GenerateJsonEndDelivery(long idDriver,String token, long idTrip) {

        ContainerEndDelivery containerEndDelivery = new ContainerEndDelivery();
        containerEndDelivery.PointDeliveryEnd_Token = token;
        containerEndDelivery.PointDeliveryEnd_IdDriver = idDriver;
        containerEndDelivery.PointDeliveryEnd_IdTrip = idTrip;

        List<ContainerEndStand_left> listContainerEndStand_left = new ArrayList<>();
        List<ContainerEndStand_receive> listContainerEndStand_receive = new ArrayList<>();
        containerEndDelivery.PointDeliveryEnd_ListLeftStand = db.GetContainerEndStand_left(idDriver,idTrip,3);
        containerEndDelivery.PointDeliveryEnd_ListReciveStand = db.GetContainerEndStand_receive(idDriver,idTrip,2,Constants.AddbyUser_False);
        containerEndDelivery.PointDeliveryEnd_ListReciveStand_ByAddUser = db.GetContainerEndStand_receive(idDriver,idTrip,2,Constants.AddbyUser_True);

        Gson gson = new Gson();
        Type type = new TypeToken<ContainerEndDelivery>() {}.getType();
        return gson.toJson(containerEndDelivery, type);
    }

}
