package com.eso.DI.Infrastructure;

import com.eso.Common.Constants;
import com.eso.Container.ContainerWindowStand_left;
import com.eso.Container.ContainerWindowStand_recive;
import com.eso.DI.MyApplication;
import com.eso.Database.DatabaseHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

public class OperationsOnStandDI {
    private DatabaseHelper db;

    @Inject
    public OperationsOnStandDI(){
        db = new DatabaseHelper(MyApplication.getAppContext());
    }

    public boolean UpdateListContainerWindowStand_left(List<ContainerWindowStand_left> ListContainerWindowStand_left) {
        boolean result = false;
        if (ListContainerWindowStand_left != null && !ListContainerWindowStand_left.isEmpty()) {
            for (ContainerWindowStand_left containerWindowStand_left : ListContainerWindowStand_left) {
                db.updateStandLeft(containerWindowStand_left);
            }
            result = true;
        }
        else
        {
            result = false;
        }
        return result;
    }
    public boolean UpdateListContainerWindowStand_recive(List<ContainerWindowStand_recive> ListContainerWindowStand_recive) {

        boolean result = false;
        if (ListContainerWindowStand_recive != null && !ListContainerWindowStand_recive.isEmpty()) {
            for (ContainerWindowStand_recive ContainerWindowStand_recive : ListContainerWindowStand_recive) {
                db.updateStandRecive(ContainerWindowStand_recive);
            }
            result = true;
        }
        else
        {
            result = false;
        }
        return result;
    }

    public boolean AddStandbyUser_recive(int Id, int tripid, int idclient, int isaddbyuser, int userId, String namewindow, Date date, int status) {

        boolean result = false;
        long k = db.insertStand(Id,tripid,idclient,isaddbyuser,userId,namewindow, date, status);
        return result;
    }

    public int GetLastIdClient_recive() {

        return db.GetLastIdClient()+1;
    }

}
