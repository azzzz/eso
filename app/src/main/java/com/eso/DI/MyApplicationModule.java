package com.eso.DI;

import com.eso.Activites.AdapterWindowStands_Left;
import com.eso.Activites.AdapterWindowStands_Recive;
import com.eso.Activites.DriverAdressClientActivty;
import com.eso.Activites.MainActivity;
import com.eso.Activites.Menu.MenuActivty;
import com.eso.Activites.StandsForSpecificTrips;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MyApplicationModule {
    @ContributesAndroidInjector
    abstract MenuActivty contributeActivityInjector();
    @ContributesAndroidInjector
    abstract DriverAdressClientActivty contributeDriverAdressClientActivty();
    @ContributesAndroidInjector
    abstract StandsForSpecificTrips contributeStandsForSpecificTrips();
}
