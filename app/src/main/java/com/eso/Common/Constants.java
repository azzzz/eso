package com.eso.Common;

import java.text.SimpleDateFormat;

public class Constants {
    public static final long ESO_VERSION = 114;

    public static String WebServiceURL =  "";
    public static final String STRING_EMPTY = "";
    public static final int AddbyUser_False = 0;
    public static final int AddbyUser_True = 1;
    //ApiService responses
    public static String ApiServiceURL_LogIn = "https://api.oknovid.pl:8443/kierowca.php?action=logowanie&login=xxxx&pass=yyyy";
    public static String ServerDateFormat =  "yyyy-MM-dd HH:mm:ss";
    public static final String Response_Connect = "ok";
    public static final String Response_Error = "error";
    public static final String Response_NoResponse = "No response";
    public static final String Response_ConnectionFailure = "";
    public static final String DB_PATH_eso = "/data/user/0/com.edwardvanraak.materialbarcodescanner/databases/eso.db";
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    //

    //FileLoadSave
    public static String LoginPasswordNameFile = "LoginPassword.txt";
    public static String UserTokenNameFile = "UserToken.txt";
    public static String ListStandsNameFile = "ListStands.txt";

    //BundleSave
    public static String Bundle_UserId = "Bundle_UserId";
    public static String Bundle_pointDeliveryIdClient= "Bundle_PointDeliveryIdClient";
    public static String Bundle_TripId= "Bundle_TripId";
    //
    //JSON Login API
    public static final String Json_statement = "komunikat";
    public static final String Json_login = "logowanie";
    public static final String Json_driver_id = "id_kierowca";
    public static final String Json_token = "token";
    public static final String Json_driver_name = "kierowca";
    //

    //JSON Trip API
    public static final String Json_trip_id = "id_wyjazd";
    public static final String Json_date_trip = "data_wyjazdu";
    //public static final String Json_statement = "komunikat";
    public static final String Json_point = "punkty";
    public static final String Json_client_id = "id_klient";
    public static final String Json_clientName = "klient";
    public static final String Json_adress = "adres";
    public static final String Json_delivery_code = "dostawa_kod";
    public static final String Json_delivery_town = "dostawa_miasto";
    public static final String Json_delivery_street = "dostawa_ulica";
    public static final String Json_delivery_newConstruction = "dostawa_nowabudowa";
    public static final String Json_id_country = "id_kraj";
    public static final String Json_delivery_country = "dostawa_kraj";
    public static final String Json_stands = "stojaki";
    public static final String Json_standsclient = "stojaki_klienta";
    public static final String Json_stands_id = "id_stojaki";
    public static final String Json_stands_name = "stojak_nazwa";
    //

    //Status Stand
    public static final int StatusStand_free = 0;
    public static final int StatusStand_client = 1;
    public static final int StatusStand_delivered = 2;

    public static  String SimpleAdressClient = "[" +
            "{\"Id\":1," +
            "\"PostalCode\":\"87-100 \"," +
            "\"Town\":\"Toruń\"," +
            "\"NameStreet\":\"ul. Aleksandrowska 6-10 \"," +
            "\"NewConstruction\":false," +
            "\"Descryption\":\" Arek 723417147 \"" +
            "}"+","+

            "{\"Id\":2," +
            "\"PostalCode\":\"85-000 \"," +
            "\"Town\":\"Warszawa\"," +
            "\"NameStreet\":\"Fordon 6 \"," +
            "\"NewConstruction\":false," +
            "\"Descryption\":\"Adrian 723417147 \"" +
            "}"+","+

            "{\"Id\":3," +
            "\"PostalCode\":\"85-000 \"," +
            "\"Town\":\"Zakopane\"," +
            "\"NameStreet\":\"Fordon 6 \"," +
            "\"NewConstruction\":false," +
            "\"Descryption\":\"Zbigniew 723417147 \"" +
            "}"+","+


            "{\"Id\":4," +
            "\"PostalCode\":\"85-000 \"," +
            "\"Town\":\"Wałcz\"," +
            "\"NameStreet\":\"Fordon 6 \"," +
            "\"NewConstruction\":false," +
            "\"Descryption\":\"Magda 723417147 \"" +
            "}"+","+


            "{\"Id\":5," +
            "\"PostalCode\":\"85-000 \"," +
            "\"Town\":\"Gdańsk\"," +
            "\"NameStreet\":\"Fordon 6 \"," +
            "\"NewConstruction\":false," +
            "\"Descryption\":\"Przemek 723417147 \"" +
            "}"+","+


            "{\"Id\":6," +
            "\"PostalCode\":\"85-000 \"," +
            "\"Town\":\"Chełmno\"," +
            "\"NameStreet\":\"Fordon 6 \"," +
            "\"NewConstruction\":false," +
            "\"Descryption\":\"Ania 723417147 \"" +
            "}"+","+



            "{\"Id\":7," +
            "\"PostalCode\":\"85-000 \"," +
            "\"Town\":\"Hel\"," +
            "\"NameStreet\":\"Fordon 6 \"," +
            "\"NewConstruction\":false," +
            "\"Descryption\":\"Marcin 723417147 \"" +
            "}"+","+



            "{\"Id\":8," +
            "\"PostalCode\":\"85-000 \"," +
            "\"Town\":\"Kalisz\"," +
            "\"NameStreet\":\"Fordon 6 \"," +
            "\"NewConstruction\":false," +
            "\"Descryption\":\"Tomek 723417147 \"" +
            "}"+","+



            "{\"Id\":9," +
            "\"PostalCode\":\"85-000 \"," +
            "\"Town\":\"Kielce\"," +
            "\"NameStreet\":\"Fordon 6 \"," +
            "\"NewConstruction\":false," +
            "\"Descryption\":\"Andrzej 723417147 \"" +
            "}"+","+

            "{\"Id\":10," +
            "\"PostalCode\":\"87-140 \"," +
            "\"Town\":\"Chełmża\"," +
            "\"NameStreet\":\"Sikorskiego 20 \"," +
            "\"NewConstruction\":true," +
            "\"Descryption\":\"Tomek 723417147 \"" +
            "}"+
            "]";



    public static  String SimpleStand = "[" +
            "{\"Id\":1," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" A01 \"" +
            "}"+","+

            "{\"Id\":2," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" B02 \"" +
            "}"+","+

            "{\"Id\":3," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" C03 \"" +
            "}"+","+

            "{\"Id\":4," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" D04 \"" +
            "}"+","+

            "{\"Id\":5," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" E05 \"" +
            "}"+","+

            "{\"Id\":6," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" F06 \"" +
            "}"+","+

            "{\"Id\":7," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" G07 \"" +
            "}"+","+

            "{\"Id\":8," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" H08 \"" +
            "}"+","+


            "{\"Id\":10," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" I09 \"" +
            "}"+","+

            "{\"Id\":11," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" J10 \"" +
            "}"+","+

            "{\"Id\":12," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" K11 \"" +
            "}"+","+



            "{\"Id\":13," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" L12 \"" +
            "}"+","+

            "{\"Id\":14," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" M13 \"" +
            "}"+","+

            "{\"Id\":15," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" N14 \"" +
            "}"+","+



            "{\"Id\":16," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" M15 \"" +
            "}"+","+

            "{\"Id\":17," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" O16 \"" +
            "}"+","+

            "{\"Id\":18," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" P17 \"" +
            "}"+","+



            "{\"Id\":19," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" R18 \"" +
            "}"+","+

            "{\"Id\":20," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" S19 \"" +
            "}"+","+

            "{\"Id\":21," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" T20 \"" +
            "}"+","+



            "{\"Id\":22," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" W21 \"" +
            "}"+","+

            "{\"Id\":23," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" X22 \"" +
            "}"+","+

            "{\"Id\":24," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y23 \"" +
            "}"+","+

            "{\"Id\":25," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y24 \"" +
            "}"+","+

            "{\"Id\":26," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y25 \"" +
            "}"+","+

            "{\"Id\":27," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y26 \"" +
            "}"+","+

            "{\"Id\":28," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y27 \"" +
            "}"+","+

            "{\"Id\":29," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y28 \"" +
            "}"+","+


            "{\"Id\":30," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y29 \"" +
            "}"+","+


            "{\"Id\":31," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y30 \"" +
            "}"+","+


            "{\"Id\":32," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y31 \"" +
            "}"+","+


            "{\"Id\":33," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y32 \"" +
            "}"+","+


            "{\"Id\":34," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y33 \"" +
            "}"+","+


            "{\"Id\":35," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y34 \"" +
            "}"+","+


            "{\"Id\":36," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y35 \"" +
            "}"+","+


            "{\"Id\":37," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y38 \"" +
            "}"+","+


            "{\"Id\":38," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y37 \"" +
            "}"+","+



            "{\"Id\":39," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y38 \"" +
            "}"+","+



            "{\"Id\":40," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y39 \"" +
            "}"+","+


            "{\"Id\":41," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y40 \"" +
            "}"+","+


            "{\"Id\":42," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Y41 \"" +
            "}"+","+

            "{\"Id\":43," +
            "\"Stand_TripId\":\"0 \"," +
            "\"Stand_NameWindow\":\" Z44 \"" +
            "}"+
            "]";

    public static  String SimpleNewResponseWithIdDelivery ="{\n" +
            "   \"id_wyjazd\": \"2673\",\n" +
            "   \"data_wyjazdu\": \"2020-10-19\",\n" +
            "   \"komunikat\": \"ok\",\n" +
            "   \"punkty\": {\n" +
            "      \"1\": {\n" +
            "         \"klient\": \"EBELING\",\n" +
            "         \"id_klient\": \"70\",\n" +
            "         \"adres\": {\n" +
            "            \"adres_typ\": \"d\",\n" +
            "            \"id_adres\": 999,\n" +
            "            \"dostawa_kod\": \"31848\",\n" +
            "            \"dostawa_miasto\": \"Bad Münder\",\n" +
            "            \"dostawa_ulica\": \"Am Bahnhof 1\",\n" +
            "            \"dostawa_nowabudowa\": \"f\"\n" +
            "         },\n" +
            "         \"stojaki_klienta\": [\n" +
            "            {\n" +
            "               \"id_stojaki\": \"1188\",\n" +
            "               \"stojak_nazwa\": \"S73\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"id_stojaki\": \"2411\",\n" +
            "               \"stojak_nazwa\": \"X4\"\n" +
            "            }\n" +
            "         ]\n" +
            "      },\n" +
            "      \"2\": {\n" +
            "         \"klient\": \"OTTOWICX\",\n" +
            "         \"id_klient\": \"40\",\n" +
            "         \"adres\": {\n" +
            "            \"adres_typ\": \"d\",\n" +
            "            \"id_adres\": 999,\n" +
            "            \"dostawa_kod\": \"30453\",\n" +
            "            \"dostawa_miasto\": \"Hannover\",\n" +
            "            \"dostawa_ulica\": \"Gottinger Chaussee 12-14\",\n" +
            "            \"dostawa_nowabudowa\": \"f\"\n" +
            "         },\n" +
            "         \"stojaki_klienta\": [\n" +
            "            {\n" +
            "               \"id_stojaki\": \"1885\",\n" +
            "               \"stojak_nazwa\": \"R652\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"id_stojaki\": \"2164\",\n" +
            "               \"stojak_nazwa\": \"R800\"\n" +
            "            }\n" +
            "         ]\n" +
            "      },\n" +
            "      \"3\": {\n" +
            "         \"klient\": \"ATZUMER\",\n" +
            "         \"id_klient\": \"1193\",\n" +
            "         \"adres\": {\n" +
            "            \"adres_typ\": \"d\",\n" +
            "            \"id_adres\": 999,\n" +
            "            \"dostawa_kod\": \"38302\",\n" +
            "            \"dostawa_miasto\": \"Wolfenbüttel\",\n" +
            "            \"dostawa_ulica\": \"Schlickerberg 17\",\n" +
            "            \"dostawa_nowabudowa\": \"f\"\n" +
            "         },\n" +
            "         \"stojaki_klienta\": []\n" +
            "      },\n" +
            "      \"5\": {\n" +
            "         \"klient\": \"STOLPE\",\n" +
            "         \"id_klient\": \"1330\",\n" +
            "         \"adres\": {\n" +
            "            \"adres_typ\": \"d\",\n" +
            "            \"id_adres\": 999,\n" +
            "            \"dostawa_kod\": \"\",\n" +
            "            \"dostawa_miasto\": \"02991 Lauta\",\n" +
            "            \"dostawa_ulica\": \"Ernst-Thaelmann-Str. 22A\",\n" +
            "            \"dostawa_nowabudowa\": \"f\"\n" +
            "         },\n" +
            "         \"stojaki_klienta\": [\n" +
            "            {\n" +
            "               \"id_stojaki\": \"1386\",\n" +
            "               \"stojak_nazwa\": \"R259\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"id_stojaki\": \"2037\",\n" +
            "               \"stojak_nazwa\": \"R674\"\n" +
            "            }\n" +
            "         ]\n" +
            "      },\n" +
            "      \"6\": {\n" +
            "         \"klient\": \"NOACK\",\n" +
            "         \"id_klient\": \"450\",\n" +
            "         \"adres\": {\n" +
            "            \"adres_typ\": \"d\",\n" +
            "            \"id_adres\": 999,\n" +
            "            \"dostawa_kod\": \"\",\n" +
            "            \"dostawa_miasto\": \"02953 Kromlau\",\n" +
            "            \"dostawa_ulica\": \"Am See 48\",\n" +
            "            \"dostawa_nowabudowa\": \"f\"\n" +
            "         },\n" +
            "         \"stojaki_klienta\": []\n" +
            "      }\n" +
            "   },\n" +
            "   \"stojaki\": [\n" +
            "      {\n" +
            "         \"id_stojaki\": \"67\",\n" +
            "         \"stojak_nazwa\": \"C16\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"id_stojaki\": \"1793\",\n" +
            "         \"stojak_nazwa\": \"E101\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"id_stojaki\": \"157\",\n" +
            "         \"stojak_nazwa\": \"G7\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"id_stojaki\": \"2060\",\n" +
            "         \"stojak_nazwa\": \"R742\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"id_stojaki\": \"2372\",\n" +
            "         \"stojak_nazwa\": \"R926\"\n" +
            "      }\n" +
            "   ]\n" +
            "}";


}
