package com.eso.Common;

public enum EnumLoginPermission {

    DRIVER("Driver", 0),
    STOREKEEPER("Storekeeper", 1);

    private String stringValue;
    private int intValue;

    private EnumLoginPermission(String toString, int value) {
        stringValue = toString;
        intValue = value;
    }

    @Override
    public String toString() {
        return stringValue;
    }
}


