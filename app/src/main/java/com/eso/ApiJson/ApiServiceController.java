package com.eso.ApiJson;

import android.content.Context;

import com.eso.ApiJson.Interfaces.IDriverAdressClient;
import com.eso.ApiJson.Interfaces.IEndTrip;
import com.eso.ApiJson.Interfaces.IGetIdDriver;
import com.eso.ApiJson.Interfaces.IGetVersion;
import com.eso.ApiJson.Interfaces.ILogin;
import com.eso.ApiJson.Tasks.AsyncTaskDriverAdressClient;
import com.eso.ApiJson.Tasks.AsyncTaskEndTrip;
import com.eso.ApiJson.Tasks.AsyncTaskGetIdDriver;
import com.eso.ApiJson.Tasks.AsyncTaskGetVersion;
import com.eso.ApiJson.Tasks.AsyncTaskLogin;
import com.eso.Common.Constants;

import org.json.JSONObject;


public class ApiServiceController
{
    public static void GetIdDriver(Context context, String ApiServiceURL, IGetIdDriver parent)
    {
        AsyncTaskGetIdDriver task = new AsyncTaskGetIdDriver();
        task.Init();
        task.SetContext(context);
        task.SetParameters(ApiServiceURL, parent);
        try
        {
            task.execute(null, null, null);
        }
        catch (Exception e)
        {

        }
    }


    public static void GetLogin(Context context, String UserName,String Password, ILogin parent)
    {
        AsyncTaskLogin task = new AsyncTaskLogin();
        task.Init();
        task.SetContext(context);
        task.SetParameters(UserName,Password, parent);
        try
        {
            task.execute(null, null, null);
        }
        catch (Exception e)
        {

        }
    }

    public static void GetDriverAdressClient(Context context,int user_id, String Token, IDriverAdressClient parent)
    {
        AsyncTaskDriverAdressClient task = new AsyncTaskDriverAdressClient();
        task.Init();
        task.SetContext(context);
        task.SetParameters(user_id,Token, parent);
        try
        {
            task.execute(null, null, null);
        }
        catch (Exception e)
        {

        }
    }

    public static void GetVersion(Context context, IGetVersion parent)
    {
        AsyncTaskGetVersion task = new AsyncTaskGetVersion();
        task.Init();
        task.SetContext(context);
        task.SetParameters(parent);
        try
        {
            task.execute(null, null, null);
        }
        catch (Exception e)
        {

        }
    }

    public static void EndTrip(String dataJson, Context context, IEndTrip parent)
    {
        AsyncTaskEndTrip task = new AsyncTaskEndTrip();
        task.Init();
        task.SetContext(context);
        task.SetParameters(dataJson,parent);
        try
        {
            task.execute(null, null, null);
        }
        catch (Exception e)
        {

        }
    }
}
