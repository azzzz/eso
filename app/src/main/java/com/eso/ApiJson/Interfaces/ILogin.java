package com.eso.ApiJson.Interfaces;


import com.eso.ApiJson.Tasks.AsyncTaskLogin;

/**
 * Created by Marcin on 2017-10-04.
 */

public interface ILogin
{
    public void OnAsyncTaskLogin(AsyncTaskLogin caller);
    ////////////////////
    ////////////////////
    
}
