package com.eso.ApiJson.Interfaces;


import com.eso.ApiJson.Tasks.AsyncTaskGetIdDriver;

/**
 * Created by Marcin on 2017-10-04.
 */

public interface IGetIdDriver
{
    public void OnAsyncTaskGetIdDriver(AsyncTaskGetIdDriver caller);
    ////////////////////
    ////////////////////
    
}
