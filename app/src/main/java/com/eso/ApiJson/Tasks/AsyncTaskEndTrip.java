package com.eso.ApiJson.Tasks;

import android.util.Log;
import android.widget.Toast;

import com.eso.ApiJson.Interfaces.IEndTrip;
import com.eso.Common.Constants;
import com.eso.R;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import static com.eso.Common.Constants.STRING_EMPTY;


public class AsyncTaskEndTrip extends AsyncTaskBaseWebApi
{
    public String Result = STRING_EMPTY;
    public IEndTrip Parent;
    public Date ServerDate;
    public String DataJson;

    public void SetParameters(String dataJson, IEndTrip parent) {
        //this.URL = "https://api.oknovid.pl:8443/kierowca.php?action=logowanie&login=testowydriver&pass=haslo";
        this.URL = "https://api.oknovid.pl:8443/kierowca.php?action=zakonczwyjazd";
        this.DataJson = dataJson;
        this.Parent = parent;
    }

    @Override
    public String Execute() {
        //String email = emailText.getText().toString();
        // Do some validation here

        String result = STRING_EMPTY;

        HttpURLConnection httpURLConnection = null;
        try {
            trustEveryone();
            java.net.URL url = new URL(URL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.writeBytes(this.DataJson);
            os.flush();
            os.close();
            //Log.i("STATUS", String.valueOf(conn.getResponseCode()));
            //Log.i("MSG", conn.getResponseMessage());
            result = String.valueOf(conn.getResponseMessage());
            conn.disconnect();
        } catch (Exception e) {
            result = Constants.Response_NoResponse;
        }
        return result;
    }


    @Override
    protected void onPostExecute(String result)
    {
        this.Result = result;
        WriteResponse(this.Result);
        this.TaskCompleted = true;
        Parent.OnAsyncTaskEndTrip(this);
        return;
    }

    public void PostExecute(String result)
    {

        this.Result = result;
        WriteResponse(this.Result);
        this.TaskCompleted = true;
        Parent.OnAsyncTaskEndTrip(this);
        return;
    }


    private void trustEveryone() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    context.getSocketFactory());
        } catch (Exception e) { // should never happen
            e.printStackTrace();
        }
    }
}