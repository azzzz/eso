package com.eso.ApiJson.Tasks;

import com.eso.ApiJson.Interfaces.IGetVersion;
import com.eso.ApiJson.Interfaces.ILogin;
import com.eso.Common.Constants;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import static com.eso.Common.Constants.STRING_EMPTY;


public class AsyncTaskGetVersion extends AsyncTaskBaseWebApi
{
    public String Result = STRING_EMPTY;
    public IGetVersion Parent;
    public Date ServerDate;

    public void SetParameters( IGetVersion parent)
    {
        this.URL = "https://api.oknovid.pl:8443/kierowca.php?action=getversion";
        this.Parent = parent;
    }

    @Override
    public String Execute()
    {
        //String email = emailText.getText().toString();
        // Do some validation here

        String result = STRING_EMPTY;


        try {
            trustEveryone();
            java.net.URL url = new URL(URL);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                result = stringBuilder.toString();
            }
            finally{
                urlConnection.disconnect();
            }
        }
        catch(Exception e) {
            result = Constants.Response_NoResponse;
        }

        return result;
    }




    @Override
    protected void onPostExecute(String result)
    {
        this.Result = result;
        WriteResponse(this.Result);
        this.TaskCompleted = true;
        Parent.OnAsyncTaskGetVersion(this);
        return;
    }

    public void PostExecute(String result)
    {

        this.Result = result;
        WriteResponse(this.Result);
        this.TaskCompleted = true;
        Parent.OnAsyncTaskGetVersion(this);
        return;
    }


    private void trustEveryone() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    context.getSocketFactory());
        } catch (Exception e) { // should never happen
            e.printStackTrace();
        }
    }

}