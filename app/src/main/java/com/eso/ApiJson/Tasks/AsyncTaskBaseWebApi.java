package com.eso.ApiJson.Tasks;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.eso.Common.Constants;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.eso.Common.Constants.STRING_EMPTY;


public class AsyncTaskBaseWebApi
        extends AsyncTask<Void, String, String>
{


    protected String URL;
    protected String NAMESPACE;
    protected String SOAP_ACTION;
    protected String METHOD;

    protected Context context = null;
    public Boolean TaskCompleted;
    public Boolean TaskSuccess;

    public void SetContext(Context context)
    {
        this.context = context;
    }

    public void Init()
    {
        this.TaskCompleted = false;
        this.TaskSuccess = false;
        //this.URL = URL;

    }


    public String Execute()
    {
        String result = STRING_EMPTY;


        try {
            java.net.URL url = new URL(URL);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                result = stringBuilder.toString();
            }
            finally{
                urlConnection.disconnect();
            }
        }
        catch(Exception e) {
            Log.e("ERROR", e.getMessage(), e);
            result = Constants.Response_NoResponse;
        }

        return result;
    }


    protected String doInBackground(Void... params)
    {
        String result = Execute();
        return result;
    }

    protected void WriteResponse(String result)
    {
        if (this.context != null)
        {
            try
            {
                //Toast.makeText(this.context, result, Toast.LENGTH_LONG).show();
            }
            catch (Exception e)
            {

            }
        }
    }

    @Override
    protected void onProgressUpdate(String... progress)
    {
        return;
    }

    @Override
    protected void onPostExecute(String result)
    {
        this.TaskCompleted = true;
        return;
    }
}