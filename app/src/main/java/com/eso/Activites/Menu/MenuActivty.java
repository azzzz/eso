package com.eso.Activites.Menu;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.eso.Activites.DriverAdressClientActivty;
import com.eso.Activites.Login;
import com.eso.ApiJson.ApiServiceController;
import com.eso.ApiJson.Interfaces.IDriverAdressClient;
import com.eso.ApiJson.Interfaces.IEndTrip;
import com.eso.ApiJson.Tasks.AsyncTaskDriverAdressClient;
import com.eso.ApiJson.Tasks.AsyncTaskEndTrip;
import com.eso.Common.Constants;
import com.eso.Container.ContainerEndDelivery;
import com.eso.Container.ContainerPointDelivery;
import com.eso.Container.ContainerStandTrip;
import com.eso.Container.ContainerVersion;
import com.eso.DI.Infrastructure.DataNetworkDI;
import com.eso.DI.Infrastructure.LoadAndSaveDI;
import com.eso.Database.DatabaseHelper;
import com.eso.Database.model.Version;
import com.eso.R;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;


public class MenuActivty extends AppCompatActivity implements IDriverAdressClient, IEndTrip {

    @Inject
    DataNetworkDI _dataNetworkDI;
    @Inject
    LoadAndSaveDI _loadAndSaveDI;
    String TokenUser;
    int IdUser;
    private DatabaseHelper db;
    MenuActivty thisActivity;
    Long IdTrip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        this.thisActivity = this;
        db = new DatabaseHelper(this);
        IdUser = Integer.valueOf(_loadAndSaveDI.LoadUserToken().get(0));
        TokenUser = db.GetTokenByIdUser(IdUser);
        IdTrip = db.GetTripByIdUser(IdUser);
    }

    public void Btn_StarTrip(View v) throws IOException {
        ApiServiceController.GetDriverAdressClient(getApplicationContext(),IdUser,TokenUser,thisActivity );
    }

    public void Btn_ListTrip(View v) throws IOException {
        OpenMainActivty(DriverAdressClientActivty.class);
    }

    public void Btn_EndTrip(View v) throws IOException {

        AlertDialog diaBox = AskOption();
        diaBox.show();
    }

    public void Btn_LogOut(View v) throws IOException {
        OpenMainActivty(Login.class);
    }



    @Override
    public void onBackPressed() {

    }

    @Override
    public void AsyncTaskDriverAdressClient(AsyncTaskDriverAdressClient caller) {

        if (!caller.Result.equals(Constants.Response_NoResponse)) {
            _dataNetworkDI.GeneratelistdataContainerPointDelivery(caller.Result,IdUser);
            List<ContainerStandTrip> vv = _dataNetworkDI.GenerateContainerStandTrip_ClientStand(caller.Result,IdUser);
            List<ContainerStandTrip>  ddd =_dataNetworkDI.GenerateContainerStandTrip_FreeStand(caller.Result,IdUser);
            Toast.makeText(getApplicationContext(),  R.string.Save_toast, Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(getApplicationContext(), R.string.OfflineMode, Toast.LENGTH_LONG).show();
        }

    }

    private void OpenMainActivty(Class<?> cls) {
        Intent m = new Intent(MenuActivty.this, cls);
        startActivity(m);
        finish();
    }

    @Override
    public void OnAsyncTaskEndTrip(AsyncTaskEndTrip caller) {

        if(caller.Result.equals("OK")) {
            Toast.makeText(getApplicationContext(), "Wysłano informacje do bazy danych! SERWER:" + caller.Result, Toast.LENGTH_LONG).show();
            db.DeleteAllPointDelivery(IdUser);
            db.DeleteAllStands(IdUser);
        }

        else if (caller.Result.equals(Constants.Response_NoResponse)) {
            Toast.makeText(getApplicationContext(), R.string.OfflineMode, Toast.LENGTH_LONG).show();
        }
        else if (caller.Result.equals("")) {
            Toast.makeText(getApplicationContext(), R.string.OfflineMode, Toast.LENGTH_LONG).show();
        }
    }

    //region  Alert

    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                // set message, title, and icon
                .setTitle(R.string.EndTrip)
                .setMessage(R.string.InfoAlert)
                //.setIcon(R.drawable.addnew)

                .setPositiveButton(R.string.InfoAlertYES, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        String EndTripJson = _dataNetworkDI.GenerateJsonEndDelivery(IdUser,TokenUser,IdTrip);
                        ApiServiceController.EndTrip(EndTripJson,getApplicationContext(),thisActivity);
                        dialog.dismiss();
                    }

                })
                .setNegativeButton(R.string.InfoAlertNO, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();

        return myQuittingDialogBox;
    }
    //endregion
}
