package com.eso.Activites;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.eso.Activites.Menu.MenuActivty;
import com.eso.ApiJson.ApiServiceController;
import com.eso.ApiJson.Interfaces.IDriverAdressClient;
import com.eso.ApiJson.Tasks.AsyncTaskDriverAdressClient;
import com.eso.Common.Constants;
import com.eso.Common.FileLoadSave;
import com.eso.Common.RecyclerItemClickListener;
import com.eso.Container.ContainerPointDelivery;
import com.eso.Container.ContainerStandTrip;
import com.eso.DI.Infrastructure.DataNetworkDI;
import com.eso.DI.Infrastructure.LoadAndSaveDI;
import com.eso.Database.DatabaseHelper;
import com.eso.R;
import com.google.gson.reflect.TypeToken;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.xml.transform.Result;

import dagger.android.AndroidInjection;

public class DriverAdressClientActivty extends AppCompatActivity {

    @Inject
    DataNetworkDI dataNetworkDI;
    @Inject
    LoadAndSaveDI _loadAndSaveDI;


    DriverAdressClientActivty thisActivity;

    private RecyclerView mRVContainerAdressClient;
    private AdapterPointDelivery mAdapter;
    MaterialSearchView serchView;
    String TokenUser;
    int IdUser;

    ContainerStandTrip containerStandTrip = new ContainerStandTrip();
    List<ContainerStandTrip> dataContainerStandTrip_client=new ArrayList<>();
    ContainerPointDelivery containerPointDelivery = new ContainerPointDelivery();
    List<ContainerPointDelivery> dataContainerPointDelivery=new ArrayList<>();

    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_adress_client_activty);
        this.thisActivity = this;

        TextView TextviewUserName = (TextView) findViewById(R.id.textUserName);

        serchView = (MaterialSearchView) findViewById(R.id.search_view);
        mRVContainerAdressClient = (RecyclerView) findViewById(R.id.RV_AdresClientList);
        db = new DatabaseHelper(this);

        IdUser = Integer.valueOf(_loadAndSaveDI.LoadUserToken().get(0));
        TokenUser = db.GetTokenByIdUser(IdUser);
        TextviewUserName.setText(db.GetNameByIdUser(IdUser));
        GeneratePointDelivery(db.getAllPointDelivery(IdUser));
        dataContainerPointDelivery =db.getAllPointDelivery(IdUser);
        String TitleActionBar = getString(R.string.ListPoint) +" "+ db.GetDateStartTrip(db.GetTripByIdUser(IdUser));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(TitleActionBar);
        toolbar.setTitleTextColor(Color.WHITE);



        serchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {
                //GenerateDataFromJson(result2);

            }
        });


        serchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<ContainerPointDelivery> dataContainerPointDelivery=new ArrayList<>();
                dataContainerPointDelivery =db.getAllPointDelivery(IdUser);
                if(newText !=null && !newText.isEmpty())
                {
                    List <ContainerPointDelivery> ListFoundItem = new ArrayList<>();
                    for(ContainerPointDelivery item:dataContainerPointDelivery)
                    {
                        if(item.PointDelivery_NameClient.toLowerCase().contains(newText.toLowerCase()) |item.PointDelivery_Town.toLowerCase().contains(newText.toLowerCase()))
                            ListFoundItem.add(item);
                    }
                    SearchResultByData(ListFoundItem);
                }
                else
                {
                    SearchResultByData(dataContainerPointDelivery);
                }
                return true;
            }
        });

        mRVContainerAdressClient.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), mRVContainerAdressClient ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        OpenMainActivty(StandsForSpecificTrips.class,dataContainerPointDelivery.get(position).PointDelivery_IdClient,dataContainerPointDelivery.get(position).PointDelivery_IdTrip);
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        MenuItem item = menu.findItem(R.id.action_search);
        serchView = (MaterialSearchView) findViewById(R.id.search_view);
        serchView.setMenuItem(item);
        return true;
    }


    public void SearchResultByData(List <ContainerPointDelivery> listdataContainerPointDelivery) {

        dataContainerPointDelivery =  listdataContainerPointDelivery;
        mAdapter = new AdapterPointDelivery(DriverAdressClientActivty.this, listdataContainerPointDelivery);
        mRVContainerAdressClient.setAdapter(mAdapter);
        mRVContainerAdressClient.setLayoutManager(new LinearLayoutManager(DriverAdressClientActivty.this));
    }

    public void onBackPressed() {
        OpenMainActivty(MenuActivty.class);
    }

    private void OpenMainActivty(Class<?> cls) {

        Intent m = new Intent(DriverAdressClientActivty.this, cls);
        startActivity(m);
        finish();

    }

    public void showMessage(String title,String Message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

    public void GeneratePointDelivery(List<ContainerPointDelivery> listdataContainerPointDelivery) {

        SearchResultByData(listdataContainerPointDelivery);
    }

    private void OpenMainActivty(Class<?> cls, int pointDeliveryIdClient, int tripId) {

        Intent m = new Intent(DriverAdressClientActivty.this, cls);
        m.putExtra(Constants.Bundle_pointDeliveryIdClient, pointDeliveryIdClient);
        m.putExtra(Constants.Bundle_TripId, tripId);
        startActivity(m);
        finish();
    }

}
