package com.eso.Activites;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.eso.Common.Constants;
import com.eso.Common.FileLoadSave;
import com.eso.Common.Parameters;
import com.eso.Container.ContainerWindowStand_left;
import com.eso.Container.ContainerWindowStand_recive;
import com.eso.Container.StandInterface.IStandLists;
import com.eso.DI.Infrastructure.OperationsOnStandDI;
import com.eso.DI.MyApplication;
import com.eso.Database.DatabaseHelper;
import com.eso.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

public class AdapterWindowStands_Left extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements IStandLists {

    private List<ContainerWindowStand_left> mModelList_left;
    private int ClientId;
    private int TripId;
    private Context context;
    private LayoutInflater inflater;
    private ItemClickListener mClickListener;
    private DatabaseHelper db;
    private int IdUser;
    StandsForSpecificTrips adapterCallback ;

    public AdapterWindowStands_Left(StandsForSpecificTrips standsForSpecificTrips, List<ContainerWindowStand_left> modelList,int clientId, int tripId) {
        this.context=standsForSpecificTrips;
        inflater= LayoutInflater.from(context);
        mModelList_left = modelList;
        ClientId =clientId;
        TripId = tripId;
        db = new DatabaseHelper(MyApplication.getAppContext());
        this.adapterCallback = standsForSpecificTrips;
    }

    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_windows_stands_left, parent,false);
        AdapterWindowStands_Left.MyViewHolder holder=new AdapterWindowStands_Left.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        final ContainerWindowStand_left modelContainerWindowStands_left = mModelList_left.get(position);
        final AdapterWindowStands_Left.MyViewHolder myHolder_left= (AdapterWindowStands_Left.MyViewHolder) holder;
        myHolder_left.textView.setText(modelContainerWindowStands_left.Stand_NameWindow);
        myHolder_left.textView.setBackgroundColor(modelContainerWindowStands_left.Stand_Status ==0 ? Color.TRANSPARENT : Color.TRANSPARENT) ;
        myHolder_left.textView.setBackgroundColor(modelContainerWindowStands_left.Stand_Status ==3 ? Color.MAGENTA : Color.TRANSPARENT) ;
        //myHolder_left.textView.setBackgroundColor(modelContainerWindowStands_left.Stand_Status ==0 && modelContainerWindowStands_left.Stand_ClientId !=0 ? Color.YELLOW : Color.TRANSPARENT) ;

        myHolder_left.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                modelContainerWindowStands_left.setStand_isSelected(!modelContainerWindowStands_left.isStand_isSelected());
                myHolder_left.textView.setBackgroundColor(modelContainerWindowStands_left.isStand_isSelected() ? Color.GREEN : Color.TRANSPARENT);

                if(mModelList_left.get(position).Stand_Status == 0)
                {
                    mModelList_left.get(position).Stand_ClientId = ClientId ;
                    mModelList_left.get(position).Stand_DateClick = Constants.DATE_FORMAT.format(Calendar.getInstance().getTime()) ;
                    mModelList_left.get(position).Stand_Status = 3 ; //3
                    mModelList_left.get(position).Stand_TripId = TripId ;
                }
                else if (mModelList_left.get(position).Stand_Status >= 0)
                {
                    mModelList_left.get(position).Stand_ClientId = 0 ;
                    mModelList_left.get(position).Stand_DateClick = Constants.DATE_FORMAT.format(Calendar.getInstance().getTime()) ;
                    mModelList_left.get(position).Stand_Status = 0 ;
                    mModelList_left.get(position).Stand_TripId = TripId ;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mModelList_left.size();
    }

    @Override
    public List<ContainerWindowStand_left> GetListContainerWindowStand_left() {
        return mModelList_left;
    }

    @Override
    public List<ContainerWindowStand_recive> GetListContainerWindowStand_recive() {
        return null;
    }


    public  class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView textView;

        // create constructor to get widget reference
        public MyViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.textNameWindowStand_left);
            itemView.setOnClickListener(this);
        }
        // Click event for all items
        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


}