package com.eso.Activites;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.eso.Common.Constants;
import com.eso.Common.FileLoadSave;
import com.eso.Common.Parameters;

import com.eso.Container.ContainerPointDelivery;
import com.eso.Container.ContainerStandTrip;
import com.eso.Container.ContainerWindowStand_left;
import com.eso.Container.ContainerWindowStand_recive;
import com.eso.Container.StandInterface.IStandLists;
import com.eso.DI.Infrastructure.DataNetworkDI;
import com.eso.DI.Infrastructure.OperationsOnStandDI;
import com.eso.DI.MyApplication;
import com.eso.Database.DatabaseHelper;
import com.eso.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

public class AdapterWindowStands_Recive extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements IStandLists {

    private int TripId;
    private int ClientId;
    private List<ContainerWindowStand_recive> mModelList_recive;
    private Context context;
    private LayoutInflater inflater;
    private ItemClickListener mClickListener;
    private DatabaseHelper db;
    private int IdUser;
    StandsForSpecificTrips adapterCallback ;

    public AdapterWindowStands_Recive(StandsForSpecificTrips standsForSpecificTrips, List<ContainerWindowStand_recive> modelList, int clientId, int tripId) {
        this.context = standsForSpecificTrips;
        inflater = LayoutInflater.from(context);
        mModelList_recive = new ArrayList<>();
        mModelList_recive = modelList;
        ClientId = clientId;
        TripId = tripId;
        db = new DatabaseHelper(MyApplication.getAppContext());
        IdUser = Integer.valueOf(LoadUserToken().get(0));
        this.adapterCallback = standsForSpecificTrips;
    }

    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.container_windows_stands_recive, parent, false);
        AdapterWindowStands_Recive.MyViewHolder holder = new AdapterWindowStands_Recive.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        final ContainerWindowStand_recive modelContainerWindowStands_recive = mModelList_recive.get(position);
        final AdapterWindowStands_Recive.MyViewHolder myHolder_recive = (AdapterWindowStands_Recive.MyViewHolder) holder;
        myHolder_recive.textView_recive.setText(modelContainerWindowStands_recive.Stand_NameWindow);

        myHolder_recive.textView_recive.setBackgroundColor(modelContainerWindowStands_recive.Stand_Status ==1 ? Color.TRANSPARENT : Color.TRANSPARENT) ;
        myHolder_recive.textView_recive.setBackgroundColor(modelContainerWindowStands_recive.Stand_Status ==2 ? Color.YELLOW : Color.TRANSPARENT) ;

       // myHolder_recive.textView_recive.setBackgroundColor(modelContainerWindowStands_recive.isStand_isSelected() ? Color.GREEN : Color.TRANSPARENT);
        myHolder_recive.textView_recive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                modelContainerWindowStands_recive.setStand_isSelected(!modelContainerWindowStands_recive.isStand_isSelected());
                myHolder_recive.textView_recive.setBackgroundColor(modelContainerWindowStands_recive.isStand_isSelected() ? Color.GREEN : Color.TRANSPARENT);

                if (mModelList_recive.get(position).Stand_Status == 1) {
                    mModelList_recive.get(position).Stand_ClientId = ClientId;
                    mModelList_recive.get(position).Stand_DateClick = Constants.DATE_FORMAT.format(Calendar.getInstance().getTime());
                    mModelList_recive.get(position).Stand_Status = 2;
                    mModelList_recive.get(position).Stand_TripId = TripId;
                }
                else if (mModelList_recive.get(position).Stand_Status >= 0) {
                    mModelList_recive.get(position).Stand_ClientId = ClientId;
                    mModelList_recive.get(position).Stand_DateClick = Constants.DATE_FORMAT.format(Calendar.getInstance().getTime());
                    mModelList_recive.get(position).Stand_Status = 1;
                    mModelList_recive.get(position).Stand_TripId = TripId;
                }

            }
        });
    }

    @Override
    public int getItemCount() {

        return mModelList_recive.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView textView_recive;

        // create constructor to get widget reference
        public MyViewHolder(View itemView) {
            super(itemView);
            textView_recive = (TextView) itemView.findViewById(R.id.textNameWindowStand_recive);
            itemView.setOnClickListener(this);
        }

        // Click event for all items
        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    private List<String> LoadUserToken() {
        FileLoadSave fileLoadSave = new FileLoadSave();
        return fileLoadSave.Read(MyApplication.getAppContext(), Constants.UserTokenNameFile);
    }

    @Override
    public List<ContainerWindowStand_left> GetListContainerWindowStand_left() {
        return null;
    }

    @Override
    public List<ContainerWindowStand_recive> GetListContainerWindowStand_recive() {
        return mModelList_recive;
    }

}