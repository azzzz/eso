package com.eso.Activites;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.eso.Common.Constants;
import com.eso.Common.FileLoadSave;
import com.eso.Common.Parameters;
import com.eso.Container.ContainerWindowStand_left;
import com.eso.Container.ContainerWindowStand_recive;
import com.eso.DI.Infrastructure.LoadAndSaveDI;
import com.eso.DI.Infrastructure.OperationsOnStandDI;
import com.eso.DI.MyApplication;
import com.eso.Database.DatabaseHelper;
import com.eso.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;


public class StandsForSpecificTrips extends AppCompatActivity  {

    @Inject
    OperationsOnStandDI _operationsOnStandDI;
    @Inject
    LoadAndSaveDI _loadAndSaveDI;

    public String result;

    List<ContainerWindowStand_left> dataContainerWindowStands_left = new ArrayList<>();
    private RecyclerView mRecyclerView_left;
    private RecyclerView.Adapter mAdapter_left;

    //List<ContainerWindowStand_recive> dataContainerWindowStands_recive=new ArrayList<>();
    private RecyclerView mRecyclerView_recive;
    private RecyclerView.Adapter mAdapter_recive;

    //MaterialSearchView serchViewWindowStand;
    FloatingActionButton fabAddNewWindowStand;
    Toolbar toolbarLeft;
    Toolbar toolbarRecive;

    private DatabaseHelper db;
    int IdUser;
    int PointDeliveryIdClient;
    int TripId;
    AdapterWindowStands_Left adapterWindowStands_Left;
    AdapterWindowStands_Recive adapterWindowStands_recive;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stand_for_specific_trips);

        fabAddNewWindowStand = (FloatingActionButton) findViewById(R.id.fab_add_new_window_stand);

        toolbarLeft = (Toolbar) findViewById(R.id.toolbar_window_stand_left);
        setSupportActionBar(toolbarLeft);
        getSupportActionBar().setTitle(R.string.left);
        toolbarLeft.setTitleTextColor(Color.WHITE);

        toolbarRecive = (Toolbar) findViewById(R.id.toolbar_window_stand_receive);
        setSupportActionBar(toolbarRecive);
        getSupportActionBar().setTitle(R.string.receive);
        toolbarRecive.setTitleTextColor(Color.WHITE);

        db = new DatabaseHelper(this);
        IdUser = Integer.valueOf(_loadAndSaveDI.LoadUserToken().get(0));
        PointDeliveryIdClient = getIntent().getIntExtra(Constants.Bundle_pointDeliveryIdClient, 0);
        TripId = getIntent().getIntExtra(Constants.Bundle_TripId, 0);

        GenerateStand_left(db.getAllStands(IdUser,0,3));
        GenerateStand_recive(db.getAllStands_recive(IdUser,PointDeliveryIdClient,1,2));

        fabAddNewWindowStand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddNewWindowStand();
            }
        });
    }

    public void GenerateStand_left(List<ContainerWindowStand_left> dataContainerWindowStands_left) {

        mRecyclerView_left = (RecyclerView) findViewById(R.id.RV_WindowStands_left);
        adapterWindowStands_Left = new AdapterWindowStands_Left(this, dataContainerWindowStands_left, PointDeliveryIdClient, TripId);
        LinearLayoutManager manager_left = new GridLayoutManager(StandsForSpecificTrips.this, 2);
        mRecyclerView_left.setHasFixedSize(true);
        mRecyclerView_left.setLayoutManager(manager_left);
        mRecyclerView_left.setAdapter(adapterWindowStands_Left);
    }

    public void GenerateStand_recive(List<ContainerWindowStand_recive> dataContainerWindowStands_recive) {

        mRecyclerView_recive = (RecyclerView) findViewById(R.id.RV_WindowStands_receive);
        adapterWindowStands_recive = new AdapterWindowStands_Recive(this, dataContainerWindowStands_recive, PointDeliveryIdClient, TripId);
        LinearLayoutManager manager_recive = new GridLayoutManager(StandsForSpecificTrips.this, 2);
        mRecyclerView_recive.setHasFixedSize(true);
        mRecyclerView_recive.setLayoutManager(manager_recive);
        mRecyclerView_recive.setAdapter(adapterWindowStands_recive);
    }

    public void onBackPressed() {
        OpenMainActivty(DriverAdressClientActivty.class);
    }

    private void OpenMainActivty(Class<?> cls) {

        Intent m = new Intent(StandsForSpecificTrips.this, cls);
        startActivity(m);
        finish();
    }

    private void AddNewWindowStand() {

        LayoutInflater li = LayoutInflater.from(this);
        final View promptsView = li.inflate(R.layout.layout_dialog_add_window_stand, null, false);
        final DialogAddWindowStand alert = new DialogAddWindowStand(this);
        final EditText EditTxT_Password = (EditText) promptsView.findViewById(R.id.EditText_namestand);
        alert.setContentView(promptsView);
        alert.setCancelable(true);

        alert.findViewById(R.id.btn_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        alert.findViewById(R.id.btn_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _operationsOnStandDI.AddStandbyUser_recive(_operationsOnStandDI.GetLastIdClient_recive(),TripId,PointDeliveryIdClient,Constants.AddbyUser_True,IdUser,EditTxT_Password.getText().toString(), Calendar.getInstance().getTime(), 1);
                GenerateStand_left(db.getAllStands(IdUser,0,3));
                GenerateStand_recive(db.getAllStands_recive(IdUser,PointDeliveryIdClient,1,2));
                alert.dismiss();
            }
        });
        alert.show();
    }

    public void Btn_SaveListStand(View v) throws IOException {

        List<ContainerWindowStand_recive> ListContainerWindowStand_recive = adapterWindowStands_recive.GetListContainerWindowStand_recive();
        boolean L_recve = _operationsOnStandDI.UpdateListContainerWindowStand_recive(ListContainerWindowStand_recive);

        List<ContainerWindowStand_left> ListContainerWindowStand_left = adapterWindowStands_Left.GetListContainerWindowStand_left();
        boolean L_left = _operationsOnStandDI.UpdateListContainerWindowStand_left(ListContainerWindowStand_left);

        OpenMainActivty(DriverAdressClientActivty.class);
        Toast.makeText(getApplicationContext(), R.string.saved, Toast.LENGTH_LONG).show();

        //GenerateStand_recive(db.getAllStands_recive(IdUser, PointDeliveryIdClient, 1));
        //GenerateStand_left(db.getAllStands_left(IdUser, 0));





    }

}
