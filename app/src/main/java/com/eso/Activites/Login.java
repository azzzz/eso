package com.eso.Activites;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.eso.Activites.Menu.MenuActivty;
import com.eso.ApiJson.ApiServiceController;
import com.eso.ApiJson.Interfaces.IGetVersion;
import com.eso.ApiJson.Interfaces.ILogin;
import com.eso.ApiJson.Tasks.AsyncTaskGetVersion;
import com.eso.ApiJson.Tasks.AsyncTaskLogin;
import com.eso.Common.Constants;
import com.eso.Common.FileLoadSave;
import com.eso.Common.InternetConnection;
import com.eso.Container.ContainerSignIn;
import com.eso.Container.ContainerVersion;
import com.eso.Database.DatabaseHelper;
import com.eso.R;
import com.eso.UpdateApp.DownloadApk;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.security.ProviderInstaller;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.google.android.gms.common.GoogleApiAvailability.*;


// TODO ID ORDER pytanie do JK odnośnie stojaków

public class Login extends AppCompatActivity implements ILogin, IGetVersion {

    private static final int MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 1001;
    private static EditText EditLogin;
    private static EditText EditPassword;
    private Button Button_LogIn;
    private CheckBox CheckBox_hide_pass;
    private View loadingPanel;
    Login thisActivity;
    public String Login;
    public String Password;
    ContainerSignIn SignInData = new ContainerSignIn();

    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.thisActivity = this;
        // doDBCheck();

        try {


            //doDBCheck();
            requestWriteExternalStoragePermission();
            if (InternetConnection.checkConnection(getApplicationContext())) {
                ApiServiceController.GetVersion(getApplicationContext(), thisActivity);

            } else {
                Toast.makeText(getApplicationContext(), "Brak Neta", Toast.LENGTH_LONG).show();
            }

            RegisterControls();
            CheckBoxHidePassword();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        }
        try {
            LoadAndSetLoginPassword();

        } catch (Exception e) {

        }

        try {
            db = new DatabaseHelper(this);

        } catch (Exception e) {
            String a = e.toString();
        }


    }

    public void RegisterControls() {
        setContentView(R.layout.activity_login);

        EditLogin = (EditText) findViewById(R.id.Edt_Login);
        EditPassword = (EditText) findViewById(R.id.Edt_Password);
        Button_LogIn = (Button) findViewById(R.id.Btn_Log_In);
        CheckBox_hide_pass = (CheckBox) findViewById(R.id.CH_show_hide_password);
        Button_LogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ButtonOnClik_Log_In(v);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        this.loadingPanel = findViewById(R.id.loadingPanel);
    }

    public void ButtonOnClik_Log_In(View v) throws IOException {

        HideKeyboard();
        loadingPanel.setVisibility(View.VISIBLE);
        this.Login = EditLogin.getText().toString();
        this.Password = EditPassword.getText().toString();
        //WebServiceController.SignIn(getApplicationContext(), this.Login,  this.Password,thisActivity);
        ApiServiceController.GetLogin(getApplicationContext(), Login, Password, thisActivity);
    }

    public void ButtonOnClik_Update(View v) throws IOException {
        //checkWriteExternalStoragePermission();

        Intent m = new Intent(Login.this, MenuActivty.class);
        startActivity(m);
        finish();
    }

    private void HideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }


    private void CheckBoxHidePassword() {

        CheckBox_hide_pass = (CheckBox) findViewById(R.id.CH_show_hide_password);
        CheckBox_hide_pass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                          @Override
                                                          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                                                              if (isChecked) {

                                                                  CheckBox_hide_pass.setText(R.string.hide_pass);


                                                                  EditPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                                                                  EditPassword.setTransformationMethod(HideReturnsTransformationMethod
                                                                          .getInstance());// show password

                                                              } else {
                                                                  CheckBox_hide_pass.setText(R.string.Checkbox_show_pass);


                                                                  EditPassword.setInputType(InputType.TYPE_CLASS_TEXT
                                                                          | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                                                                  EditPassword.setTransformationMethod(PasswordTransformationMethod
                                                                          .getInstance());// hide password

                                                              }

                                                          }
                                                      }

        );
    }

    private void OpenMainActivty(Class<?> cls, int user_id) {

        Intent m = new Intent(Login.this, cls);
        m.putExtra(Constants.Bundle_UserId, user_id);
        startActivity(m);
        finish();
    }

    private void LoadAndSetLoginPassword() {

        FileLoadSave fileLoadSave = new FileLoadSave();
        List<String> RememberLoginPassword = fileLoadSave.Read(getApplicationContext(), Constants.LoginPasswordNameFile);
        EditLogin.setText(RememberLoginPassword.get(0));
        EditPassword.setText(RememberLoginPassword.get(1));
    }

    private void SaveLoginPassword(String Login, String Password) {
        FileLoadSave fileLoadSave = new FileLoadSave();
        List<String> SaveLoginPasword = new ArrayList<String>();
        SaveLoginPasword.add(Login);
        SaveLoginPasword.add(Password);
        fileLoadSave.Save(SaveLoginPasword, getApplicationContext(), Constants.LoginPasswordNameFile, false);
    }

    private void SaveUser(String UserId) {
        FileLoadSave fileLoadSave = new FileLoadSave();
        List<String> SaveUserToken = new ArrayList<String>();
        SaveUserToken.add(UserId);
        fileLoadSave.Save(SaveUserToken, getApplicationContext(), Constants.UserTokenNameFile, false);
    }

    @Override
    public void OnAsyncTaskLogin(AsyncTaskLogin caller) {

        if (caller.Result.equals(Constants.Response_NoResponse)) {



            if(db.IfSignIn_OfflineMode(this.Login,this.Password)==true)
            {
                SaveUser(String.valueOf(db.GetIdUserbyLoginPassword(this.Login,this.Password)));
                //OpenMainActivty(DriverAdressClientActivty.class,db.GetIdUserbyLoginPassword(this.Login,this.Password) );
                OpenMainActivty(MenuActivty.class,db.GetIdUserbyLoginPassword(this.Login,this.Password) );
                loadingPanel.setVisibility(View.GONE);
            }
            else
            {
                Toast.makeText(getApplicationContext(), R.string.ResponsePasswordFail, Toast.LENGTH_LONG).show();
                loadingPanel.setVisibility(View.GONE);
            }

        } else {
            GetDateFromJSON_JK(caller.Result);

            if (SignInData.user_status.equals(Constants.Response_Connect)) {
                SaveLoginPassword(Login, Password);

                if (db.checkIfIdUserExists(SignInData.user_id) == true) {
                    db.insertUser(SignInData.user_id, SignInData.user_name,Login, Password, SignInData.user_token, SignInData.user_statement, SignInData.user_status, "0");
                    //OpenMainActivty(DriverAdressClientActivty.class, SignInData.user_id);
                    OpenMainActivty(MenuActivty.class, SignInData.user_id);
                    SaveUser(String.valueOf(SignInData.user_id));
                    loadingPanel.setVisibility(View.GONE);
                } else {
                    db.updateToken(SignInData.user_id, SignInData.user_token);
                    //OpenMainActivty(DriverAdressClientActivty.class, SignInData.user_id);
                    OpenMainActivty(MenuActivty.class, SignInData.user_id);
                    SaveUser(String.valueOf(SignInData.user_id));
                    loadingPanel.setVisibility(View.GONE);
                }

            } else if (SignInData.user_status.equals(Constants.Response_Error)) {
                Toast.makeText(getApplicationContext(), R.string.ResponsePasswordFail, Toast.LENGTH_LONG).show();
                loadingPanel.setVisibility(View.GONE);
            } else if (SignInData.user_status.equals(" ")) {
                Toast.makeText(getApplicationContext(), R.string.OfflineMode, Toast.LENGTH_LONG).show();
                loadingPanel.setVisibility(View.GONE);
            }
        }


    }

    public void GetDateFromJSON_JK(String result) {

        if (result.equals(Constants.Response_NoResponse)) {
            Toast.makeText(Login.this, R.string.NoResult, Toast.LENGTH_LONG).show();

        } else {

            try {

                JSONObject resultJson = new JSONObject(result);

                SignInData.user_statement = resultJson.getString(Constants.Json_statement);
                SignInData.user_status = resultJson.getString(Constants.Json_login);
                SignInData.user_id = resultJson.getInt(Constants.Json_driver_id);
                SignInData.user_token = resultJson.getString(Constants.Json_token);
                SignInData.user_name = resultJson.getString(Constants.Json_driver_name);
                //SignInData.user_password = resultJson.getString("user_password");
                //SignInData.user_permission = resultJson.getInt("user_permission");

            } catch (JSONException e) {

            }

        }
    }

    private void checkWriteExternalStoragePermission() {

        if (ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            /** If we have permission than we can Start the Download the task **/
            downloadTask();
        } else {
            /** If we don't have permission than requesting  the permission **/
            requestWriteExternalStoragePermission();
        }
    }

    private void downloadTask() {

        DownloadApk downloadApk = new DownloadApk(Login.this);
        //downloadApk.startDownloadingApk("//https://github.com/Piashsarker/AndroidAppUpdateLibrary/raw/master/app-debug.apk");
        downloadApk.startDownloadingApk("https://github.com/271026/eso/raw/main/app-debug.apk");
    }

    private void requestWriteExternalStoragePermission() {
        //region Aby updater dzialal poprawnie w wersjach od Android 4.1 (API level 16) api 16 do Android 5.0 (API level 21)
        // wymagane jest konto na google play i zaktualizwanie aplakcji "Usługi Google Play"
        // otrzymamy o tym odpowiednie powiadomnie

        if (Build.VERSION.SDK_INT < 21) {
            try {
                ProviderInstaller.installIfNeeded(this);

            } catch (Exception e) {

                //Crashes the app when accessing the getMessage
                switch (getInstance().isGooglePlayServicesAvailable(this)){
                    case ConnectionResult.SERVICE_MISSING:
                        getInstance().getErrorDialog(this,ConnectionResult.SERVICE_MISSING,0).show();
                        break;
                    case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                        getInstance().getErrorDialog(this,ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED,0).show();
                        break;
                    case ConnectionResult.SERVICE_DISABLED:
                        getInstance().getErrorDialog(this, ConnectionResult.SERVICE_DISABLED,0).show();
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + getInstance().isGooglePlayServicesAvailable(this));
                }
                Log.i("error", e.getMessage());
            }




        }
        //endregion

        if (ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Login.this,  new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
        } else{
            ActivityCompat.requestPermissions(Login.this,new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }


    @Override
    public void OnAsyncTaskGetVersion(AsyncTaskGetVersion caller) {
        if (!caller.Result.equals(Constants.Response_NoResponse))
        {
            String jsonString =caller.Result; //http request
            ContainerVersion data =new ContainerVersion() ;
            Gson gson = new Gson();
            data= gson.fromJson(jsonString,ContainerVersion.class);
            if(Constants.ESO_VERSION>=data.Version)
            {
                Toast.makeText(getApplicationContext(), "Nie pobiera nowej wersji", Toast.LENGTH_LONG).show();
            }
            else if(Constants.ESO_VERSION<data.Version)
            {
                checkWriteExternalStoragePermission();
            }
        }
        else {

        }


    }
}
