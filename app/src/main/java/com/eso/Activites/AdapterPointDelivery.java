package com.eso.Activites;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eso.Container.ContainerPointDelivery;
import com.eso.R;

import java.util.Collections;
import java.util.List;

public class AdapterPointDelivery extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private LayoutInflater inflater;
    List<ContainerPointDelivery> data= Collections.emptyList();
    ContainerPointDelivery current;
    int currentPos=0;//

    // create constructor to initialize context and data sent from MainActivity
    public AdapterPointDelivery(Context context, List<ContainerPointDelivery> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }

    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_adress_client, parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }

    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        MyHolder myHolder= (MyHolder) holder;
        ContainerPointDelivery current=data.get(position);

        myHolder.textLnNameClient.setText(current.PointDelivery_NameClient);
        myHolder.textTownSet.setText(current.PointDelivery_Town);
        myHolder.textOfficeCodeSet.setText(current.PointDelivery_Code);
        myHolder.textCountrySet.setText(current.PointDelivery_Country);
        myHolder.textAdresOrderSet.setText(current.PointDelivery_Street);
        myHolder.textNewConstructionSet.setText(current.PointDelivery_NewConstruction);
    }

    // return total item from List
    @Override
    public int getItemCount() {
        return data.size();
    }


    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView textLnNameClient;
        TextView textTownSet;
        TextView textOfficeCodeSet;
        TextView textCountrySet;
        TextView textAdresOrderSet;
        TextView textNewConstructionSet;

        // create constructor to get widget reference
        public MyHolder(View itemView) {
            super(itemView);

            textLnNameClient= (TextView) itemView.findViewById(R.id.textLnNameClient);
            textTownSet= (TextView) itemView.findViewById(R.id.textTownSet);
            textOfficeCodeSet= (TextView) itemView.findViewById(R.id.textOfficeCodeSet);
            textCountrySet= (TextView) itemView.findViewById(R.id.textCountrySet);
            textAdresOrderSet= (TextView) itemView.findViewById(R.id.textAdresOrderSet);
            textNewConstructionSet= (TextView) itemView.findViewById(R.id.textNewConstructionSet);


            itemView.setOnClickListener(this);
        }

        // Click event for all items
        @Override
        public void onClick(View v) {

        }

    }

}
