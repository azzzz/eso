package com.eso.Database.model;

/**
 * Created by ravi on 20/02/18.
 */

public class PointDelivery {
    public static final String TABLE_NAME = "pointdelivery";


    public static final String COLUMN_IdINCREMENT = "idincrement";
    public static final String COLUMN_IdTrip = "idtrip";
    public static final String COLUMN_DateTrip = "datetrip";
    public static final String COLUMN_DescriptionTrip = "descriptiontrip";
    public static final String COLUMN_Id = "id";
    public static final String COLUMN_Idclient = "idclient";
    public static final String COLUMN_NameClient = "nameclient";
    public static final String COLUMN_Code = "code";
    public static final String COLUMN_Town = "town";
    public static final String COLUMN_Street = "street";
    public static final String COLUMN_NewConstruction = "newconstruction";
    public static final String COLUMN_IdCountry = "idcountry";
    public static final String COLUMN_Country = "country";
    public static final String COLUMN_PointDeliveryUserId = "PointDeliveryUserId";


    private int idtrip;
    private String datetrip;
    private String descriptiontrip;
    private int id;
    private int idclient;
    private String nameclient;
    private String code;
    private String town;
    private String street;
    private String newconstruction;
    private int idcountry;
    private String country;
    private int pointDeliveryUserId;

    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_IdINCREMENT + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_IdTrip + " INTEGER,"
                    + COLUMN_DateTrip + " TEXT,"
                    + COLUMN_DescriptionTrip + " TEXT,"
                    + COLUMN_Id + " INTEGER,"
                    + COLUMN_Idclient + " INTEGER,"
                    + COLUMN_NameClient + " TEXT,"
                    + COLUMN_Code + " TEXT,"
                    + COLUMN_Town + " TEXT,"
                    + COLUMN_Street + " TEXT,"
                    + COLUMN_NewConstruction + " TEXT,"
                    + COLUMN_IdCountry + " INTEGER,"
                    + COLUMN_Country + " TEXT,"
                    + COLUMN_PointDeliveryUserId + " TEXT"
                    + ")";

    public PointDelivery() {
    }

    public PointDelivery(int Idtrip,String Datetrip, String Descriptiontrip, int Id,int Idclient, String Nameclient, String Code, String Town,String Street, String Newconstruction,int IdCountry,String Country, int PointDeliveryUserId ) {
        this.idtrip = Idtrip;
        this.datetrip = Datetrip;
        this.descriptiontrip = Descriptiontrip;
        this.id = Id;
        this.idclient =Idclient;
        this.nameclient = Nameclient;
        this.code = Code;
        this.town = Town;
        this.street = Street;
        this.newconstruction = Newconstruction;
        this.idcountry = IdCountry;
        this.country = Country;
        this.pointDeliveryUserId = PointDeliveryUserId;
    }

    public int getidtrip() {
        return idtrip;
    }

    public String getdatetrip() {
        return datetrip;
    }

    public String getdescriptiontrip() {
        return descriptiontrip;
    }

    public int getid() {
        return id;
    }

    public int getidclient() {
        return idclient;
    }

    public String getnameclient() {
        return nameclient;
    }

    public String getcode() {
        return code;
    }

    public String gettown() {
        return town;
    }

    public String getstreet() {
        return street;
    }

    public String getnewconstruction() {
        return newconstruction;
    }

    public int getidcountry() {
        return idcountry;
    }

    public String getcountry() {
        return country;
    }

    public int getpointDeliveryUserId() {
        return pointDeliveryUserId;
    }

}
