package com.eso.Database.model;

/**
 * Created by ravi on 20/02/18.
 */

public class User {
    public static final String TABLE_NAME = "user";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_LOGIN = "login";
    public static final String COLUMN_PASSWORD = "password";
    public static final String COLUMN_TOKEN = "token";
    public static final String COLUMN_STATEMENT = "statement";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_PERMISSION = "permission";




    private int id;
    private String name;
    private String login;
    private String password;
    private String token;
    private String statement;
    private String status;
    private String permission;

    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_LOGIN + " TEXT,"
                    + COLUMN_PASSWORD + " TEXT,"
                    + COLUMN_TOKEN + " TEXT,"
                    + COLUMN_STATEMENT + " TEXT,"
                    + COLUMN_STATUS + " TEXT,"
                    + COLUMN_PERMISSION + " TEXT"
                    + ")";

    public User() {
    }

    public User(int id, String name,String login, String password, String token, String statement, String status, String permission ) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.password = password;
        this.token = token;
        this.statement = statement;
        this.status = status;
        this.permission = permission;
    }

    public int getId() {
        return id;
    }

    public String getname() {
        return name;
    }

    public String getlogin() {
        return login;
    }

    public String getpassword() {
        return password;
    }

    public String gettoken() {
        return token;
    }

    public String getstatement() {
        return statement;
    }

    public String getstatus() {
        return status;
    }

    public String getpermission() {
        return permission;
    }


}
