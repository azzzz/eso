package com.eso.Database.model;

/**
 * Created by ravi on 20/02/18.
 */

public class Version {
    public static final String TABLE_NAME = "version";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_VERSIONNUMBER = "versionNumber";
    public static final String COLUMN_DESCRIPTION = "description";




    private int id;
    private String versionnumber;
    private String description;

    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_VERSIONNUMBER + " TEXT,"
                    + COLUMN_DESCRIPTION + " TEXT"
                    + ")";

    public Version() {
    }

    public Version(int id, String versionNumber, String description ) {
        this.id = id;
        this.versionnumber = versionNumber;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getversion() {
        return versionnumber;
    }

    public String getdescriptionversion() {
        return description;
    }



}
