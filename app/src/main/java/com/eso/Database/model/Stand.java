package com.eso.Database.model;

import java.util.Date;

/**
 * Created by ravi on 20/02/18.
 */

public class Stand {
    public static final String TABLE_NAME = "stand";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TRIPID = "tripid";
    public static final String COLUMN_CLIENTID = "clientid";
    public static final String COLUMN_ADDEDBYUSER = "addedbyuser";  // 0 - nie // 1 - tak jeśli tak to clientId nie będzie zagadzało się z tym co w bazie u JK
    public static final String COLUMN_NAMEWINDOW = "namewindow";
    public static final String COLUMN_STATUS = "status";
    // 0 - Stojaki które jadą na cieżarówce(NIEROZLICZNOE) - PELNE;
    // 1- Stojaki które są już u klienta (przypisane do danej lokalizacji)(ZOSTAWILEM); - PUSTE
    // 2 - Stojaki które odebrane od klienta(ODEBRALEM);
    // 3 - Stojaki które zostały dostaczone pod daną lokalizację(DOSTACZONE);
    public static final String COLUMN_DATE = "date"; //clicked date
    public static final String COLUMN_USERID = "userid";



    private int id;
    private int tripid;
    private int clientid;
    private int isaddbyuser;
    private int userid;
    private String namewindow;
    private String date;
    private int status;


    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_TRIPID + " INTEGER,"
                    + COLUMN_CLIENTID + " INTEGER,"
                    + COLUMN_ADDEDBYUSER + " INTEGER,"
                    + COLUMN_NAMEWINDOW + " TEXT,"
                    + COLUMN_USERID + " TEXT,"
                    + COLUMN_DATE + " TEXT,"
                    + COLUMN_STATUS + " TEXT"
                    + ")";

    public Stand() {
    }

    public Stand(int id, int tripid,int clientid,int isaddbyuser, String namewindow,String date, int status, int userId ) {
        this.id = id;
        this.tripid = tripid;
        this.clientid = clientid;
        this.isaddbyuser = isaddbyuser;
        this.userid = userId;
        this.namewindow = namewindow;
        this.date = date;
        this.status = status;

    }

    public int getId() {
        return id;
    }

    public int gettripid() {
        return tripid;
    }

    public int getClientid() {
        return clientid;
    }

    public int getIsaddbyuser() {
        return isaddbyuser;
    }

    public int getuserId() {
        return userid;
    }

    public String getnamewindow() {
        return namewindow;
    }

    public int getstatus() {
        return status;
    }

    public String getdate() {
        return date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void settripid(int tripid) {
        this.tripid = tripid;
    }

    public void setClientid(int clientid) {
        this.clientid = clientid;
    }

    public void setisaddbyuser(int isaddbyuser) {
        this.isaddbyuser = isaddbyuser;
    }

    public void setnamewindow(String namewindow) {
        this.namewindow = namewindow;
    }

    public void setstatus(int status) {
        this.status = status;
    }

    public void setdate(String date) {
        this.date = date;
    }

}
