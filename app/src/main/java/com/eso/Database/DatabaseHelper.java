package com.eso.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.eso.Common.Constants;
import com.eso.Container.ContainerEndStand_left;
import com.eso.Container.ContainerEndStand_receive;
import com.eso.Container.ContainerPointDelivery;
import com.eso.Container.ContainerPointDeliveryDTO;
import com.eso.Container.ContainerWindowStand_left;
import com.eso.Container.ContainerWindowStand_recive;
import com.eso.Database.model.PointDelivery;
import com.eso.Database.model.Stand;
import com.eso.Database.model.User;
import com.eso.Database.model.Version;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.eso.Database.model.Stand.COLUMN_CLIENTID;
import static com.eso.Database.model.Stand.COLUMN_ID;

/**
 * Created by ravi on 15/03/18.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    //SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    // Database Name
    private static final String DATABASE_NAME = "eso_db";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(User.CREATE_TABLE);
        db.execSQL(PointDelivery.CREATE_TABLE);
        db.execSQL(Stand.CREATE_TABLE);
        db.execSQL(Version.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + User.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PointDelivery.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Stand.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Version.TABLE_NAME);
        // Create tables again
        onCreate(db);
    }


    //region STANDS

    public long insertStand(int Id, int tripid, int idclient,int isaddbyuser, int userId, String namewindow, Date date, int status ) {


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, Id);
        values.put(Stand.COLUMN_TRIPID, tripid);
        values.put(Stand.COLUMN_CLIENTID, idclient);
        values.put(Stand.COLUMN_ADDEDBYUSER, isaddbyuser);
        values.put(Stand.COLUMN_USERID, userId);
        values.put(Stand.COLUMN_NAMEWINDOW, namewindow);
        values.put(Stand.COLUMN_DATE, Constants.DATE_FORMAT.format(date));
        values.put(Stand.COLUMN_STATUS, status);
        // insertUser row
        long id = db.insert(Stand.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;

    }

    public long updateStandLeft(ContainerWindowStand_left containerWindowStand_left ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, containerWindowStand_left.Stand_Id);
        values.put(Stand.COLUMN_TRIPID, containerWindowStand_left.Stand_TripId);
        values.put(Stand.COLUMN_CLIENTID, containerWindowStand_left.Stand_ClientId);
        values.put(Stand.COLUMN_ADDEDBYUSER, containerWindowStand_left.Stand_IsAddedByUser);
        values.put(Stand.COLUMN_USERID, containerWindowStand_left.Stand_UserId);
        values.put(Stand.COLUMN_NAMEWINDOW, containerWindowStand_left.Stand_NameWindow);
        values.put(Stand.COLUMN_DATE, containerWindowStand_left.Stand_DateClick);

        values.put(Stand.COLUMN_STATUS, containerWindowStand_left.Stand_Status);
        // insertUser row
        long id = db.update(Stand.TABLE_NAME, values,COLUMN_ID + "=" + containerWindowStand_left.Stand_Id,null );

        // close db connection
        db.close();

        // return newly inserted row id
        return id;

    }

    public long updateStandRecive(ContainerWindowStand_recive containerWindowStand_recive ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, containerWindowStand_recive.Stand_Id);
        values.put(Stand.COLUMN_TRIPID, containerWindowStand_recive.Stand_TripId);
        values.put(Stand.COLUMN_CLIENTID, containerWindowStand_recive.Stand_ClientId);
        values.put(Stand.COLUMN_ADDEDBYUSER, containerWindowStand_recive.Stand_IsAddedByUser);
        values.put(Stand.COLUMN_USERID, containerWindowStand_recive.Stand_UserId);
        values.put(Stand.COLUMN_NAMEWINDOW, containerWindowStand_recive.Stand_NameWindow);
        values.put(Stand.COLUMN_DATE, containerWindowStand_recive.Stand_DateClick);

        values.put(Stand.COLUMN_STATUS, containerWindowStand_recive.Stand_Status);
        // insertUser row
        long id = db.update(Stand.TABLE_NAME, values,COLUMN_ID + "=" + containerWindowStand_recive.Stand_Id,null );

        // close db connection
        db.close();

        // return newly inserted row id
        return id;

    }

    public List<ContainerWindowStand_left> getAllStands(int userId,int statusInCar,int statusLefted) {
        List<ContainerWindowStand_left> stands = new ArrayList<>();

        // Select All Query
        String selectQuery = "Select * From "+ Stand.TABLE_NAME + " where "
                + Stand.COLUMN_USERID+ "=" +"'"+userId+"'" +" AND "+ Stand.COLUMN_STATUS+ " IN "+ "('0','3');" ;
        //String selectQuery = "Select * From "+ Stand.TABLE_NAME + " where "
        //        + Stand.COLUMN_STATUS+ "=" +"'"+statusInCar+"'"+"or "
        //        + Stand.COLUMN_STATUS+ "=" +"'"+statusLefted+"'"+"and "
        //        + Stand.COLUMN_USERID+ "=" +"'"+userId+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ContainerWindowStand_left stand = new ContainerWindowStand_left();
                //Stand note = new Stand();
                stand.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                stand.settripid(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_TRIPID)));
                stand.setclientid(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_CLIENTID)));
                stand.setisadded(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_ADDEDBYUSER)));
                stand.setuserid(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_USERID)));
                stand.setnamewindow(cursor.getString(cursor.getColumnIndex(Stand.COLUMN_NAMEWINDOW)));
                stand.setdateclick(cursor.getString(cursor.getColumnIndex(Stand.COLUMN_DATE)));
                stand.setstatus(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_STATUS)));
                stands.add(stand);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return stands;
    }

    public List<ContainerWindowStand_recive> getAllStands_recive(int userId,int IdTrip, int statusClient, int statusRecived) {
        List<ContainerWindowStand_recive> stands = new ArrayList<>();

        // Select All Query
        String selectQuery = "Select * From "+ Stand.TABLE_NAME + " where "
                + Stand.COLUMN_USERID+ "=" +"'"+userId+"'" +" AND "+ Stand.COLUMN_STATUS+ " IN "+ "('1','2')" +" AND " + Stand.COLUMN_CLIENTID+ "=" +"'"+IdTrip+"'" ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ContainerWindowStand_recive stand = new ContainerWindowStand_recive();
                //Stand note = new Stand();
                stand.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                stand.settripid(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_TRIPID)));
                stand.setclientid(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_CLIENTID)));
                stand.setisadded(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_ADDEDBYUSER)));
                stand.setuserid(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_USERID)));
                stand.setnamewindow(cursor.getString(cursor.getColumnIndex(Stand.COLUMN_NAMEWINDOW)));
                stand.setdateclick(cursor.getString(cursor.getColumnIndex(Stand.COLUMN_DATE)));
                stand.setstatus(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_STATUS)));
                stands.add(stand);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return stands;
    }

    public List<ContainerEndStand_left> GetContainerEndStand_left(long userId, long IdTrip, int status) {
        List<ContainerEndStand_left> stands = new ArrayList<>();

        // Select All Query
        String selectQuery = "Select * From "+ Stand.TABLE_NAME + " where "
//                + Stand.COLUMN_TRIPID+ "=" +"'"+IdTrip+"'"
//                +"and "
                + Stand.COLUMN_USERID+ "=" +"'"+userId+"'"
                +"and "
                + Stand.COLUMN_STATUS+ "=" +"'"+status+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ContainerPointDeliveryDTO containerPointDeliveryDTO = new ContainerPointDeliveryDTO();
                containerPointDeliveryDTO = GetContainersAdresByClientId(cursor.getInt(cursor.getColumnIndex(COLUMN_CLIENTID)));
                ContainerEndStand_left stand_left = new ContainerEndStand_left();
                stand_left.ContainerEndStand_StandId = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
                stand_left.ContainerEndStand_LeavingPoint = cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_CLIENTID));
                //stand_left.ContainerEndStand_StandName = cursor.getString(cursor.getColumnIndex(Stand.COLUMN_NAMEWINDOW));
                stand_left.PointDelivery_Code =  containerPointDeliveryDTO.PointDelivery_Code;
                stand_left.PointDelivery_Town = containerPointDeliveryDTO.PointDelivery_Town;
                stand_left.PointDelivery_Street = containerPointDeliveryDTO.PointDelivery_Street;
                stand_left.PointDelivery_newConstruction = containerPointDeliveryDTO.PointDelivery_NewConstruction;
                stand_left.PointDelivery_LeavingDate = cursor.getString(cursor.getColumnIndex(Stand.COLUMN_DATE));

                stands.add(stand_left);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();
        return stands;
    }

    public List<ContainerEndStand_receive> GetContainerEndStand_receive(long userId, long IdTrip, int status,int isadduser) {
        List<ContainerEndStand_receive> stands = new ArrayList<>();

        // Select All Query
        String selectQuery = "Select * From "+ Stand.TABLE_NAME + " where "
//                + Stand.COLUMN_TRIPID+ "=" +"'"+IdTrip+"'"
//                +"and "
                + Stand.COLUMN_USERID+ "=" +"'"+userId+"'"
                +"and "
                + Stand.COLUMN_STATUS+ "=" +"'"+status+"'"
                +"and "
                + Stand.COLUMN_ADDEDBYUSER+ "=" +"'"+isadduser+"'"
                +"and "
                + COLUMN_CLIENTID+ "!=" +"'"+0+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ContainerPointDeliveryDTO containerPointDeliveryDTO = new ContainerPointDeliveryDTO();
                containerPointDeliveryDTO = GetContainersAdresByClientId(cursor.getInt(cursor.getColumnIndex(COLUMN_CLIENTID)));
                ContainerEndStand_receive stand_receive = new ContainerEndStand_receive();
                stand_receive.ContainerEndStand_StandId = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
                stand_receive.ContainerEndStand_reciveingPoint = cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_CLIENTID));
                //stand_receive.ContainerEndStand_StandName = cursor.getString(cursor.getColumnIndex(Stand.COLUMN_NAMEWINDOW));
                stand_receive.PointDelivery_newConstruction = containerPointDeliveryDTO.PointDelivery_NewConstruction;
                stand_receive.PointDelivery_Code =  containerPointDeliveryDTO.PointDelivery_Code;
                stand_receive.PointDelivery_Town = containerPointDeliveryDTO.PointDelivery_Town;
                stand_receive.PointDelivery_Street = containerPointDeliveryDTO.PointDelivery_Street;
                stand_receive.PointDelivery_reciveingDate = cursor.getString(cursor.getColumnIndex(Stand.COLUMN_DATE));
                stands.add(stand_receive);
            } while (cursor.moveToNext());
        }
        // close db connection
        db.close();
        return stands;
    }

    public boolean checkIfExistStands(int StandId, int StandTripId,int IsAddByuser,int StandClientId) {

        boolean status = false;

        //String Query = "Select * From user_tabele where user_id ='58'";

        String Query = "Select * From "+ Stand.TABLE_NAME + " where "
                + COLUMN_ID+ "=" +"'"+StandId+"'" +"and "
                + Stand.COLUMN_TRIPID+ "=" +"'"+StandTripId+"'" +"and "
                + Stand.COLUMN_ADDEDBYUSER+ "=" +"'"+IsAddByuser+"'" +"and "
                + Stand.COLUMN_CLIENTID+ "=" +"'"+StandClientId+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);
        int i = cursor.getCount();
        if(i <= 0){
            status= true;
        }
        else if (i > 0)
        {
            status= false;
        }
        cursor.close();

        return status;
    }

    public boolean checkIfExistStands_onlyStandID(int StandId, int IsAddByuser) {

        boolean status = false;

        //String Query = "Select * From user_tabele where user_id ='58'";

        String Query = "Select * From "+ Stand.TABLE_NAME + " where "
                + Stand.COLUMN_ADDEDBYUSER+ "=" +"'"+IsAddByuser+"'" +"and "
                + COLUMN_ID+ "=" +"'"+StandId+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);
        int i = cursor.getCount();
        if(i <= 0){
            status= true;
        }
        else if (i > 0)
        {
            status= false;
        }
        cursor.close();

        return status;
    }

    public int GetLastIdClient() {

        int result = 0;
        ContainerWindowStand_recive stand = new ContainerWindowStand_recive();
        // Select All Query
        String selectQuery = "Select * From "+ Stand.TABLE_NAME + " ORDER BY "+ COLUMN_ID+ " DESC LIMIT 1";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

                stand.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                stand.settripid(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_TRIPID)));
                stand.setclientid(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_CLIENTID)));
                stand.setisadded(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_ADDEDBYUSER)));
                stand.setuserid(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_USERID)));
                stand.setnamewindow(cursor.getString(cursor.getColumnIndex(Stand.COLUMN_NAMEWINDOW)));
                stand.setdateclick(cursor.getString(cursor.getColumnIndex(Stand.COLUMN_DATE)));
                stand.setstatus(cursor.getInt(cursor.getColumnIndex(Stand.COLUMN_STATUS)));
            }


        // close db connection
        db.close();

        // return notes list
        return stand.getId();
    }

    //endregion

    //region USER

    public long insertUser(int userid, String username,String userlogin, String userpassword, String usertoken, String userdescription, String userstatus, String userpermission) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(User.COLUMN_ID, userid);
        values.put(User.COLUMN_NAME, username);
        values.put(User.COLUMN_LOGIN, userlogin);
        values.put(User.COLUMN_PASSWORD, userpassword);
        values.put(User.COLUMN_TOKEN, usertoken);
        values.put(User.COLUMN_STATEMENT, userdescription);
        values.put(User.COLUMN_STATUS, userstatus);
        values.put(User.COLUMN_PERMISSION, userpermission);


        // insertUser row
        long id = db.insert(User.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;

    }


    public boolean checkIfIdUserExists(int iduser) {

        boolean status = false;

        //String Query = "Select * From user_tabele where user_id ='58'";

        String Query = "Select * From "+ User.TABLE_NAME + " where "+ User.COLUMN_ID+ "=" +"'"+iduser+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);
        int i = cursor.getCount();
        if(i <= 0){
            status= true;
        }
        else if (i > 0)
        {
            status= false;
        }
        cursor.close();

        return status;
    }


    public boolean checkIfExistPointDelivery(int IdTrip, int UserId, int Clientid) {

        boolean status = false;

        //String Query = "Select * From user_tabele where user_id ='58'";

        String Query = "Select * From "+ PointDelivery.TABLE_NAME + " where "
                + PointDelivery.COLUMN_IdTrip+ "=" +"'"+IdTrip+"'" +"and "
                + PointDelivery.COLUMN_PointDeliveryUserId+ "=" +"'"+UserId+"'" +"and "
                + PointDelivery.COLUMN_Idclient+ "=" +"'"+Clientid+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);
        int i = cursor.getCount();
        if(i <= 0){
            status= true;
        }
        else if (i > 0)
        {
            status= false;
        }
        cursor.close();

        return status;
    }

    public int updateToken(long user_id, String token) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(User.COLUMN_TOKEN, token);

        return  db.update(User.TABLE_NAME, contentValues, User.COLUMN_ID +" = " + user_id, null);
    }

    public String GetTokenByIdUser(int iduser) {

        String Token = "";
        SQLiteDatabase db = this.getWritableDatabase();


        String Query = "Select * From "+ User.TABLE_NAME + " where "+ User.COLUMN_ID+ "=" +"'"+iduser+"'";
        Cursor cursor = db.rawQuery(Query, null);

        int i = cursor.getCount();
        if(i == 0){

        }
        else if (i > 0)
        {
            cursor.moveToFirst();
            Token= cursor.getString(cursor.getColumnIndex(User.COLUMN_TOKEN));
        }
        cursor.close();

        return Token;
    }

    public String GetNameByIdUser(int iduser) {

        String Name = "";
        SQLiteDatabase db = this.getWritableDatabase();


        String Query = "Select * From "+ User.TABLE_NAME + " where "+ User.COLUMN_ID+ "=" +"'"+iduser+"'";
        Cursor cursor = db.rawQuery(Query, null);

        int i = cursor.getCount();
        if(i == 0){

        }
        else if (i > 0)
        {
            cursor.moveToFirst();
            Name= cursor.getString(cursor.getColumnIndex(User.COLUMN_NAME));
        }
        cursor.close();

        return Name;
    }

    public boolean IfSignIn_OfflineMode(String login,String pass) {

        boolean status = false;
        String Query = "Select * From "+ User.TABLE_NAME + " where "+ User.COLUMN_LOGIN+ "=" +"'"+login+"'"+ " and " + User.COLUMN_PASSWORD+ "=" +"'"+pass+"'" ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);
        int i = cursor.getCount();
        if(i <= 0){
            status= false;
        }
        else if (i > 0)
        {
            status= true;
        }
        cursor.close();

        return status;
    }

    public int GetIdUserbyLoginPassword(String login,String pass) {

        int idUser = 0;
        String Query = "Select * From "+ User.TABLE_NAME + " where "+ User.COLUMN_LOGIN+ "=" +"'"+login+"'"+ " and " + User.COLUMN_PASSWORD+ "=" +"'"+pass+"'" ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);
        int i = cursor.getCount();
        if(i == 0){

        }
        else if (i > 0)
        {
            cursor.moveToFirst();
            idUser= cursor.getInt(cursor.getColumnIndex(User.COLUMN_ID));
        }
        cursor.close();

        return idUser;
    }

    public boolean DeleteAllPointDelivery(int iduser) {

        boolean status = false;
        String Query = "Delete  From "+ PointDelivery.TABLE_NAME + " where "+ PointDelivery.COLUMN_PointDeliveryUserId+ "=" +"'"+iduser+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);
        int i = cursor.getCount();
        if(i <= 0){
            status= true;
        }
        else if (i > 0)
        {
            status= false;
        }
        cursor.close();

        return status;
    }

    public boolean DeleteAllStands(int iduser) {

        boolean status = false;
        String Query = "Delete From "+ Stand.TABLE_NAME + " where "+ Stand.COLUMN_USERID+ "=" +"'"+iduser+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);
        int i = cursor.getCount();
        if(i <= 0){
            status= true;
        }
        else if (i > 0)
        {
            status= false;
        }
        cursor.close();

        return status;
    }

    //endregion

    //region VERSION

    public long insertVersion( String VersionNumber, String descriptionVersion ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Version.COLUMN_VERSIONNUMBER, VersionNumber);
        values.put(Version.COLUMN_DESCRIPTION, descriptionVersion);

        // insertUser row
        long id = db.insert(Version.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public long GetVersion() {

        long version=0;
        SQLiteDatabase db = this.getWritableDatabase();


        int idversion = 1;
        String Query = "Select * From "+ Version.TABLE_NAME + " where "+ Version.COLUMN_ID+ "=" +"'"+idversion+"'";
        Cursor cursor = db.rawQuery(Query, null);

        int i = cursor.getCount();
        if(i == 0){

        }
        else if (i > 0)
        {
            cursor.moveToFirst();
            version= Long.parseLong(cursor.getString(cursor.getColumnIndex(Version.COLUMN_VERSIONNUMBER)));
        }
        cursor.close();

        return version;
    }

    public boolean UpdateVersion(String versionNumber) {

        boolean reslut = false;
        int idversion = 1;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Version.COLUMN_VERSIONNUMBER, versionNumber);
        int response = db.update(Version.TABLE_NAME, contentValues, Version.COLUMN_ID +" = " + idversion, null);
        if(response <= 0)
        {
            reslut= false;
        }
        else if (response > 0)
        {
            reslut= true;
        }

        return  reslut;
    }
    //endregion

    //region PointDelivery

    public List<ContainerPointDelivery> getAllPointDelivery(int idUser) {
        List<ContainerPointDelivery> pointdelivers = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + PointDelivery.TABLE_NAME + " where "+ PointDelivery.COLUMN_PointDeliveryUserId+ "=" +"'"+idUser+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                ContainerPointDelivery pointdeliver = new ContainerPointDelivery();
                pointdeliver.set_IdTrip(cursor.getInt(cursor.getColumnIndex(PointDelivery.COLUMN_IdTrip)));
                pointdeliver.set_DescriptionTrip(cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_DescriptionTrip)));
                pointdeliver.set_Id(cursor.getInt(cursor.getColumnIndex(PointDelivery.COLUMN_Id)));
                pointdeliver.set_IdClient(cursor.getInt(cursor.getColumnIndex(PointDelivery.COLUMN_Idclient)));
                pointdeliver.set_NameClient(cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_NameClient)));
                pointdeliver.set_Code(cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_Code)));
                pointdeliver.set_Town(cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_Town)));
                pointdeliver.set_Street(cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_Street)));
                pointdeliver.set_NewConstruction(cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_NewConstruction)));
                pointdeliver.set_Country(cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_Country)));
                pointdelivers.add(pointdeliver);
            } while (cursor.moveToNext());
        }

        db.close();

        // return notes list
        return pointdelivers;
    }

    public long insertPointDelivery(int Idtrip,String Datetrip, String Descriptiontrip, int Id,int idclient ,String Nameclient, String Code, String Town,String Street, String Newconstruction,String Country,int PointDeliveryUserId) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PointDelivery.COLUMN_Id, Id);
        values.put(PointDelivery.COLUMN_IdTrip, Idtrip);
        values.put(PointDelivery.COLUMN_DateTrip, Datetrip);
        values.put(PointDelivery.COLUMN_DescriptionTrip, Descriptiontrip);
        values.put(PointDelivery.COLUMN_Idclient, idclient);
        values.put(PointDelivery.COLUMN_NameClient, Nameclient);
        values.put(PointDelivery.COLUMN_Code, Code);
        values.put(PointDelivery.COLUMN_Town, Town);
        values.put(PointDelivery.COLUMN_Street, Street);
        values.put(PointDelivery.COLUMN_NewConstruction, Newconstruction);
        values.put(PointDelivery.COLUMN_Country, Country);
        values.put(PointDelivery.COLUMN_PointDeliveryUserId, PointDeliveryUserId);

        // insertUser row
        long id = db.insert(PointDelivery.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public Long GetTripByIdUser(int idUser) {

        long TripId=0;
        SQLiteDatabase db = this.getWritableDatabase();

        String Query = "Select * From " +  PointDelivery.TABLE_NAME + " where "+ PointDelivery.COLUMN_PointDeliveryUserId+ "=" +"'"+idUser+"'" ;
        Cursor cursor = db.rawQuery(Query, null);

        int i = cursor.getCount();
        if(i == 0){

        }
        else if (i > 0)
        {
            cursor.moveToFirst();
            TripId= Long.parseLong(cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_IdTrip)));
        }
        cursor.close();

        return TripId;

    }

    public ContainerPointDeliveryDTO GetContainersAdresByClientId(int clientId) {

        ContainerPointDeliveryDTO containerPointDeliveryDTO = new ContainerPointDeliveryDTO();
        long TripId=0;
        SQLiteDatabase db = this.getWritableDatabase();

        String Query = "Select * From " +  PointDelivery.TABLE_NAME  + " where "+ PointDelivery.COLUMN_Idclient+ "=" +"'"+clientId+"'";
        Cursor cursor = db.rawQuery(Query, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst())
        {
            containerPointDeliveryDTO.PointDelivery_IdClient = clientId;
            containerPointDeliveryDTO.PointDelivery_Code = cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_Code));
            containerPointDeliveryDTO.PointDelivery_NameClient = cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_NameClient));
            containerPointDeliveryDTO.PointDelivery_NewConstruction = cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_NewConstruction));
            containerPointDeliveryDTO.PointDelivery_Street = cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_Street));
            containerPointDeliveryDTO.PointDelivery_Town = cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_Town));
        }


            //TripId= Long.parseLong(cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_IdTrip)));

        cursor.close();

        return containerPointDeliveryDTO;

    }

    public String GetDateStartTrip(long idtrip) {
        String result = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select * From " +  PointDelivery.TABLE_NAME  + " where "+ PointDelivery.COLUMN_IdTrip+ "=" +"'"+idtrip+"'";
        Cursor cursor = db.rawQuery(Query, null);

        if (cursor.moveToFirst())
        {
            result = cursor.getString(cursor.getColumnIndex(PointDelivery.COLUMN_DateTrip));
        }

        return result;
    }

    //endregion

}
