WYKONANO:

1)	Autoryzacja urządzania za pomocą Loginu i Hasła.
2)	Pobieranie informacji o lokalizacjach 
3)	Pobieranie informacji o stojakach przypisanych do lokalizacji.
4)	Filtrowanie lokalizacji po Miastach
5)	Stworzenie Lokalnej bazy danych SQLITE


TO DO:

1)	Stworzenie możliwości aktualizacji oprogramowania. - aplikacja pobiera numer wersji z serwera a następnie jeśli się on nie zgadza pobiera aktualna wersje z LINK w którym będzie zawarta nowa wersja pliku APK. Pobieranie informacji o lokalizacjach 

2)	Obsługa żądań POST - Aplikacja zbiera dane na temat stojaków okiennych które zostały pozostawione w danej lokalizacji lub zabrane. Aplikacja musi tworzyć odpowiednie żądanie Typu POST aby komunikować się z bazą danych w celu zapisu informacji. Podczas użytkowania aplikacji będzie wykorzystywana lokalna baza danych (SQLite). Żądanie to będzie generowane dopiero gdy użytkownik będzie chciał wysłać zapisane dane(główny ekran aplikacji przycisk „Synchronizuj” lub „Zakończ pracę” )

JK:
Wysyłasz posta na:
https://api.oknovid.pl:8443/kierowca.php?action=zakonczwyjazd

I dane jakie tam potrzebuje:

token,

id_kierowca,
id_wyjazd,

tablica "zostawione" zawierająca id_stojaki i punkt pozostawienia + może też czas kiedy to zostało kliknięte
tablica "odebrane" - jak wyżej

Na ten moment żadnych tokenów nie sprawdzam, zapisuje sobie tylko te requesty żeby widzieć co będziesz wysyłał także możesz spokojnie testować.
Ja sprawdzałem Postman'em i działa.

Pozdrawiam.

ZAKONCZ WYJAZD
{
  "token": "23cddb05c76a1e23b6382e6f7071b1f37ed1e708",
  "id_kierowca": "20",
  "id_wyjazd": "2673",
  "zostawione":[
        {
          "id_stojaki": "1188",
          "punkt_pozostawienia_id_klient": "70",
          "adres": "Bad Münder Am Bahnhof 1",
          "data_pozostawienia": "2020-11-12 14:51:46"
        },
        {
          "id_stojaki": "2411",
          "punkt_pozostawienia_id_klient": "70",
          "adres": "Bad Münder Am Bahnhof 1",
          "data_pozostawienia": "2020-10-19 14:51:46"
        }
  ],
  "odebrane":[
        {
          "id_stojaki": "1188",
          "punkt_odebrania_id_klient": "40",
          "adres": "Bad Münder Am Bahnhof 1",
          "data_odebrania": "2020-10-19 14:51:46"
        },
        {
          "id_stojaki": "2411",
          "punkt_odebrania_id_klient": "40",
          "adres": "Bad Münder Am Bahnhof 1",
          "data_odebrania": "2020-10-19 14:51:46"
        }
  ]
}